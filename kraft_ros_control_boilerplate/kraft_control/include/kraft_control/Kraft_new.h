#ifndef KRAFT_H
#define KRAFT_H

#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <streambuf>
#include <cstring>
#include <cstdint>
#include <cmath>
#include <math.h>
#include <fcntl.h>
#include <algorithm>

#include <ros/ros.h>
#include <std_msgs/Float64.h>

// ROS parameter loading
#include <rosparam_shortcuts/rosparam_shortcuts.h>

#define _USE_MATH_DEFINES

using namespace std;

namespace ROSKraft
{
	class Kraft
	{
		public: Kraft(ros::NodeHandle& nh) : nh_(nh)
		{
			// Load rosparams
			ros::NodeHandle rpsnh(nh, name_);
			std::size_t error = 0;
			error += !rosparam_shortcuts::get(name_, rpsnh, "robot_ip", robot_ip_);

			if( !nh_.getParam(name_ + "/command_zero", CommandZero) )
				error += 1;
			if( !nh_.getParam(name_ + "/command_ticks_per_degree", CommandTicksPerDegree) )
				error += 1;
			if( !nh_.getParam(name_ + "/min_angle", MinAngle) )
				error += 1;
			if( !nh_.getParam(name_ + "/max_angle", MaxAngle) )
				error += 1;
			if( !nh_.getParam(name_ + "/angle_reached_threshold", AngleReachedThresh) )
				error += 1;
			// if( !nh_.getParam(name_ + "/slew_offset", SlewOffset) )
				// error += 1;
			if( !nh_.getParam(name_ + "/gains", Gains) )
				error += 1;
			// SlewOffset = SlewOffset;

			if( !nh_.getParam("generic_hw_control_loop/loop_hz", masterRate) )
				error += 1;

			if( !nh_.getParam(name_ + "/pid_kp", Kp) )
				error += 1;
			if( !nh_.getParam(name_ + "/pid_kd", Kd) )
				error += 1;
			if( !nh_.getParam(name_ + "/pid_ki", Ki) )
				error += 1;
			if( !nh_.getParam(name_ + "/pid_ki_limit", ki_limit) )
				error += 1;

			if (error)
				ROS_ERROR_STREAM("kraft_new.h parameter error...");
			rosparam_shortcuts::shutdownIfError(name_, error);

			// ToDo: Connect on construction for now to avoid initializing joint command interface with no feedback
			connect();

			// start();
			// closedHydraulics();
			// for (int i=0; i<10; i++){
			// 	sendIdle();
			// 	UdpRx();
			// 	usleep(20000);
			// }

			// setHydraulicsEnabled(true);
			// cout << "Initial command Values: \n";
			// for (int i=0;i<7;i++)
			// 	cout << dec << (unsigned int) Command[i] << " ";
			// cout << endl;

			// is_connected_ = true;
		}

		public: bool connect()
		{
			if (!is_connected_)
			{
				start();
				closedHydraulics();
				for (int i=0; i<10; i++){
					sendIdle();
					UdpRx();
					usleep(5000);
				}

				setHydraulicsEnabled(true);
				for (int i=0; i<400; i++){
					UdpTx();
					UdpRx();
					setCommandToCurrent();
					usleep(5000);
				}
				cout << "Initial command Values: \n";
				for (int i=0;i<7;i++)
					cout << dec << (unsigned int) Command[i] << " ";
				cout << endl;
				is_connected_ = true;
			}
			return is_connected_;
		}

		public: bool disconnect()
		{
			if (is_connected_)
			{
				is_connected_ = false;
				setHydraulicsEnabled(false);
				for (int i=0;i<10;i++) {
					sendIdle();
					UdpRx();
					usleep(5000);
				}
				if (close(UdpClient) < 0) // stop receiving and transmitting
					return false;
				UdpClient = 0;

				std::fill(current_angles_rad.begin(),current_angles_rad.end(),0);
				std::fill(target_angles_deg.begin(),target_angles_deg.end(),0);
				std::fill(TargetPots.begin(),TargetPots.end(),0);
				std::fill(Command.begin(),Command.end(),0);
				std::fill(Feedback.begin(),Feedback.end(),0);
			}
			return !is_connected_;
		}

		public: bool connected()
		{
			return is_connected_;
		}

		protected: void setCommandToCurrent()
		{
			for (int i =0; i<5; i++)
			{
				Command[i] = Feedback[i]*2;
			}
			updateTargetPots(current_angles_rad);
		}


		protected:
		// startup and shutdown of the internal node inside a roscpp program
		ros::NodeHandle nh_;

		// Name of this class
		std::string name_ = "kraft";

		// IP address of RSD
		std::string robot_ip_;

		bool is_connected_ = false;

		private:
		int TxAck=0; // good exchanges
		int TxNack=0; // bad exchanges
		int RxAck=0; // good exchanges
		int RxNack=0; // bad exchanges
		int UdpClient=0;
		int gripper_open=1;
		bool RxReady = false;
		float masterRate;
		const string JointNames[7] = {"Azimuth","Elevator","Elbow", "Pitch","Yaw","Rotate","Grip"};
		std::vector<int> Gains;
		std::vector<int> CommandMin;
		std::vector<int> CommandMaximum;
		std::vector<int> CommandZero;
		std::vector<float> CommandTicksPerDegree;
		std::vector<double> MinAngle;
		std::vector<double> MaxAngle;
		std::vector<double> AngleReachedThresh;
		std::vector<double> current_angles_rad{0, 0, 0, 0, 0, 0, 0};
		std::vector<double> target_angles_deg{0, 0, 0, 0, 0, 0, 0};  //60, -40, 100, 0, 0, 0, 0
		//double angle_rad[7] = {0, 0, 0, 0, 0, 0, 0};  //60, -40, 100, 0, 0, 0, 0
		std::vector<int> TargetPots{0, 0, 0, 0, 0, 0, 0};
		double SlewOffset = 0;

		//PID related
		int counter = 0;
		int vel[2];
		const int VelMin[2] = {0, 0};
		const int VelMax[2] = {255, 128};
		const int ZeroVel[2] = {128, 0};
		std::vector<double> Kp;
		std::vector<double> Kd;
		std::vector<double> Ki;
		std::vector<double> ki_limit;
		float error_pos[2] = {0, 0};
		float error_der[2] = {0, 0};
		float error_int[2] = {0, 0};
		float error_prev[2] = {0, 0};

		public: float angleRange(int axis)
		{
			return MaxAngle[axis] - MinAngle[axis];
		}

		public: int commandSpan(int axis)
		{
			return CommandTicksPerDegree[axis] * angleRange(axis);
			// return CommandMaximum[axis] - CommandMin[axis];
		}

		private: std::vector<int> Command{0, 0, 0, 0, 0, 0, 0}; // the number sent to RSD, 12 bit, equal to Fbk*2
		protected: std::vector<int> Feedback{0, 0, 0, 0, 0, 0, 0}; //the number returnd from RSD, 11 bit

		// These comms statistics are periodically refreshed in the LogLoop, but
		// individual periods are checked against min/max in their respective loops.
		long double SubseaPreviousRx = time(0)*1000;
		int SubseaPeriodMax = 0;
		int SubseaPeriodMin = 999;

		long double MasterPreviousUpdate = time(0)*1000;
		int MasterPeriodMax = 0;
		int MasterPeriodMin = 999;

		private: bool hydraulicsEnabled = false;

		public: bool getHydraulicsEnabled()
		{
			return hydraulicsEnabled;
		}

		public: void setHydraulicsEnabled(bool value)
		{
			if ((value) && (!hydraulicsEnabled))
			{
				for (int i =0; i<5; i++)
				{
					Command[i] = Feedback[i]*2;
				}
				// Command[5] = 128;

			}
			hydraulicsEnabled = value;
		}

		public: void shutdown()
		{
			setHydraulicsEnabled(false);
			for (int i=0;i<100;i++) {
				UdpRx();
				sendIdle();
				usleep(100);
			}

		}

		// WristSpeed is going to be used in an unexpected way - to spy on the master recv to get the MasterRate.
		public: int getWristSpeed()
		{
			return Command[5];
		}

		public: void setWristSpeed(int value)
		{
			Command[5] = value;


			long double MasterRecvNow = time(0)*1000;
			int Elapsed = (int) (MasterRecvNow - MasterPreviousUpdate);
			if (MasterPeriodMax < Elapsed)
				MasterPeriodMax = Elapsed;
			if (MasterPeriodMin > Elapsed)
				MasterPeriodMin = Elapsed;
			MasterPreviousUpdate = MasterRecvNow;
		}

		// GripForce is a value 0-128
		// 0 = open
		// 128 = closed
		// there is a neutral value which happens to be 39 or 40 depending on the day.
		public: int getGripForce()
		{
			return Command[6];
		}

		public: void setGripForce(int value)
		{
			Command[6] = value;
		}

		public: void setGripOpen(int value)
		{
			gripper_open = value;
		}

		private: bool GripOnLostComms = false;
		private: uint8_t SequenceCounter = 0x10; // set by subsea, echoed by topside

		private: int CycleCounter = 0; // counts UPD exchange cycles

		private: void start()
		{
			RxReady = true; // objects are initialized and ready for Rx Thread to use them.
			//cout << " initializing RSD comms." << endl;

			// Initialize UDP socket
			if ((UdpClient = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
				cerr << "cannot create socket" << endl;
				RxReady = false;
			}
			fcntl(UdpClient, F_SETFL, O_NONBLOCK); // set non-blocking
			/* bind to an arbitrary return address */
			/* because this is the client side, we don't care about the address */
			/* since no application will initiate communication here - it will */
			/* just send responses */
			/* INADDR_ANY is the IP address and 0 is the socket */
			/* htonl converts a long integer (e.g. address) to a network representation */
			/* htons converts a short integer (e.g. port) to a network representation */
			struct sockaddr_in myaddr;
			memset((char *)&myaddr, 0, sizeof(myaddr));
			myaddr.sin_family = AF_INET;
			myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
			myaddr.sin_port = htons(0);

			if (bind(UdpClient, (struct sockaddr *)&myaddr, sizeof(myaddr)) < 0) {
				cerr << "bind failed" << endl;
				RxReady = false;
			}

			sendWakeup();
		}

		public: void sendWakeup()
		{
			struct sockaddr_in servaddr; /* server address */
			/* fill in the server's address and data */
			memset((char*)&servaddr, 0, sizeof(servaddr));
			servaddr.sin_family = AF_INET;
			//servaddr.sin_addr.s_addr = htonl(inet_pton("192.168.10.151"));
			inet_pton(AF_INET, robot_ip_.c_str(), &(servaddr.sin_addr));
			servaddr.sin_port = htons(1500);

			//cout << "start streaming to subsea RSD . . . " << endl;
			// send a message to the server
			uint8_t WakeupBuffer[] = {0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f};
			int ret;
			ret = sendto(UdpClient, WakeupBuffer, 8, 0, (struct sockaddr *)&servaddr, sizeof(servaddr));
			if (ret < 0)
				cerr << "sendto failed\n";
			// else
				// cout << "Sent bytes: " << ret << endl;
		}

		public: void sendIdle()
		{
			struct sockaddr_in servaddr; /* server address */
			/* fill in the server's address and data */
			memset((char*)&servaddr, 0, sizeof(servaddr));
			servaddr.sin_family = AF_INET;
			//servaddr.sin_addr.s_addr = htonl(inet_pton("192.168.10.151"));
			inet_pton(AF_INET, robot_ip_.c_str(), &(servaddr.sin_addr));
			servaddr.sin_port = htons(1500);

			//cout << "start streaming to subsea RSD . . . " << endl;
			// send a message to the server
			uint8_t OutBuffer[] = { 0x55,1,0,1,1,0,1,1,0,0x80, 0x6b, 0x85, 0x8C, 0x0C, 0xF4, 0x02, 0, 0 }; // idle
			finalizePacket(OutBuffer);
			int ret;
			ret = sendto(UdpClient, OutBuffer, 18, 0, (struct sockaddr *)&servaddr, sizeof(servaddr));
			if (ret < 0)
				cerr << "sendto failed\n";
			// else
				// cout << "Sent bytes: " << ret << endl;
		}

		public: void UdpTx()
		{
			if (true)
			{
				struct sockaddr_in servaddr; /* server address */
				/* fill in the server's address and data */
				memset((char*)&servaddr, 0, sizeof(servaddr));
				servaddr.sin_family = AF_INET;
	            inet_pton(AF_INET, robot_ip_.c_str(), &(servaddr.sin_addr));
				servaddr.sin_port = htons(1500);

				// Declare some uint8_t arrays to use for UDP messages.
				//var WakeupBuffer = new uint8_t[] {0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f};
				// Note: Leave a zero uint8_t at the end for the checksum which will replace it.
				//var OutBuffer[18] = new uint8_t[18];
				//uint8_t OutBuffer[] = { 0x55,0x48,0xa4,0x99,0x6f,0x86,0x79,0x89,0x07,0x7c, 0x9b, 0x85, 0x8C, 0x0C, 0xF4, 0x12, 0, 0 };
				//uint8_t OutBuffer[] = { 0x55,1,0,1,1,0,1,1,0,0x80, 0, 0x85, 0x8C, 0x0C, 0xF4, 0x02, 0, 0 }; // idle
				//uint8_t OutBuffer[] = { 0x55,0xd4,0xa2,0xdc,0x6a,0x85,0x15,0x00,0x08,0x97, 0, 0x85, 0x8C, 0x0C, 0xF4, 0x1a, 0, 0}; // enabled
				//OutBuffer = new uint8_t[] { 0x55,0x48,0xa4,0x99,0x6f,0x86,0x79,0x89,0x07,0x7c, 0x9b, 0x85, 0x8C, 0x0C, 0xF4, 0x12, 0, 0 }; // gripping?
				//uint8_t OutBuffer[] = { 0x55, 0x01, 0x00, 0x01, 0x00, 0x01, 0x01, 0x00, 0x80, 0x4d, 0x85, 0x8c, 0x0c, 0xf4, 0x0a, 0x33, 0x75 };
				uint8_t OutBuffer[] = { 0x55, 0x31, 0x99, 0xa6, 0x17, 0x7a, 0x18, 0x64, 0x09, 0x88, 0x00, 0x85, 0x8c, 0x0c, 0xf4, 0x1a, 0x87, 0x15 };
				//uint8_t OutBuffer[] = { 0x15, 0x87, 0x1a, 0xf4, 0x0c, 0x8c, 0x85, 0x00, 0x88, 0x09, 0x64, 0x18, 0x7a, 0x17, 0xa6, 0x98, 0x0b, 0x55 };
				//int angle_values[7] = {70, 70, 40, 70, 60, 100, 0};

				//Command[5] = 128; // no rotate
				//Command[6] = 255; // close gripper

				if (true) //(!reachedGoalThresh(angle_rad))
				{
					populateOutgoingCommandPacket(OutBuffer);
					finalizePacket(OutBuffer);

					// now send
					try
					{
						CycleCounter++;
						if (sendto(UdpClient, OutBuffer, 18, 0, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)
							cerr << "sendto failed\n";
						// this will throw an exception if Ethernet is interrupted,
						// because the destination becomes unreachable.
						TxAck++;
					}
					catch(exception e)
					{
						cerr << "Send UDP Failed: " << e.what() << endl;
						this->TxNack++;
					}
				}


				// changed 5-> 8ms to see if it would reduce collisions. I think it does 1/25/2012.
				//System.Threading.Thread.Sleep(8);
				// 10 ms works fine, mono, 2012/2/2
				// 20 ms works fine, mono, 2012/2/2
				// 40 ms doesn't work at all. comms but hydraulics never enable. 2012/2/2
				// 30 ms doesn't work at all.
				// 25 ms doesn't work at all.
				// 21 ms works sometimes, freezes occasionally. 2012/2/2
				// 19 ms works reliably for a few minutes but does rarely freeze 2012/2/2
			}

		}// UdpTx

		public: bool reachedGoalThresh(double angle_rad[7])
		{
			int flag = true;
			for (int i=0; i<5; i++) {
				if (std::abs(angle_rad[i] - current_angles_rad[i]) > AngleReachedThresh[i])
					flag = false;
			}
			return flag;
		}

		public: void UdpRx()
		{
			uint8_t indata[2048] = {};
			long double RecvNow;
			int Elapsed;

			struct sockaddr_in servaddr; /* server address */
			/* fill in the server's address and data */
			memset((char*)&servaddr, 0, sizeof(servaddr));
			servaddr.sin_family = AF_INET;
			//servaddr.sin_addr.s_addr = htonl(inet_pton("192.168.10.151"));
			inet_pton(AF_INET, robot_ip_.c_str(), &(servaddr.sin_addr));
			servaddr.sin_port = htons(1500);
			socklen_t addrlen = sizeof(servaddr); /* length of addresses */
			int recvlen; /* # uint8_ts received */

			if (RxReady)
			{
				try
				{
					recvlen = recvfrom(UdpClient, indata, 2048, 0, (struct sockaddr *)&servaddr, &addrlen);
				//	cout << "recvlen: " << recvlen << endl;
					////////////rxk.Log(CycleCounter.ToString("000000 ") + prettyPrintPacket(indata) );
					if (recvlen == -1){
						ROS_ERROR_THROTTLE(1.0, "Receive failed. Error: %i\n", errno);
					}
					else if (recvlen > 16){
						SequenceCounter = indata[16];
						readFeedbackFromPkt(indata);
						RxAck++;
						RecvNow = time(0)*1000;
						Elapsed = (int) (RecvNow - SubseaPreviousRx);
						if (SubseaPeriodMax < Elapsed)
							SubseaPeriodMax = Elapsed;
						if (SubseaPeriodMin > Elapsed)
							SubseaPeriodMin = Elapsed;
						SubseaPreviousRx = RecvNow;
						// for (int i=0;i<recvlen+1;i++)
						// 	cout << hex << (unsigned int) indata[i] << " ";
						// cout << endl;
					}
					else
					{
						// if (this.EnableLogging)
						// 	kss.Log("ERR", "Rx packet too short. Ignored.");
						RxNack++;
					}
				}
				catch (exception e)
				{
					//cout << "UDP receive Failed: " << e.what() << " " << CycleCounter << endl;// (timeout/unreachable).");
					// if (this.EnableLogging)
					// 	kss.Log("ERR", "rx timeout: " + e.Message);
					RxNack++;
				}
	       }
		}

		private: void populateOutgoingCommandPacket(uint8_t *pkt)
		{
			// first set the status uint8_t, #15. Four possible states. Maybe more.
			if (hydraulicsEnabled)
			{
				if (GripOnLostComms)
					pkt[15] = 0x1a;
				else
					pkt[15] = 0x12;
			}
			else
			{
				if (GripOnLostComms)
					pkt[15] = 0x02;
				else
					pkt[15] = 0x0a;
			}

			// Wrist pid controller to get velocity command
			// wristPID();
			// Command[5] = vel[0];
			// cout << "Wrist velocity: " << vel[0] << std::endl;

			/// Now the reverse of the decoding:
			/// 		public void readCommandFromPkt(uint8_t[] pkt)

			//Command[0] = pkt[1] +  (pkt[2] & 0x0F) * 256; // // azimuth 12 bits
			//Command[1] = pkt[3] + (pkt[2] & 0xF0) * 16; // // elevator 12 bits
			//Command[2] = pkt[4] + (pkt[5] & 0x0F) * 256; // elbow command 12 bits
			//Command[3] = pkt[6] + (pkt[5] & 0xF0) * 16; // pitch command 12 bits
			//Command[4] = pkt[7] + (pkt[8] & 0x0F) * 256; // yaw 12 bits


			pkt[1] = (uint8_t) (Command[0] & 0xFF);
			pkt[2] = (uint8_t) (((Command[0] >> 8) & 0x0F) | ((Command[1] >> 4) & 0xF0));
			pkt[3] = (uint8_t) (Command[1] & 0xFF);
			pkt[4] = (uint8_t) (Command[2] & 0xFF);
			pkt[5] = (uint8_t) (((Command[2] >> 8) & 0x0F) | ((Command[3] >> 4) & 0xF0));
			pkt[6] = (uint8_t) (Command[3] & 0xFF);
			pkt[7] = (uint8_t) (Command[4] & 0xFF);
			pkt[8] = (uint8_t) ((Command[4] >> 8) & 0x0F);
			pkt[9] = (uint8_t) (Command[5] & 0xFF); // rotate 8 bits
			pkt[10] = (uint8_t) (Command[6] & 0xFF); // gripper 8 bits

			// cout << "Command: " << Command[5] << "\n";

			//Console.WriteLine("Grip Command = " + Command[6]);

			// Set gains
			// Gains are 7 bit numbers with max value 127 (0x7F)
			// except wrist yaw is only 4 bits with max value 15 (0x0F)
			pkt[11] = (uint8_t) ( ( Gains[0]       & 0x7F) | ((Gains[1] & 0x01) << 7) );
			pkt[12] = (uint8_t) ( ((Gains[1] >> 1) & 0x3F) | ((Gains[2] & 0x03) << 6) );
			pkt[13] = (uint8_t) ( ((Gains[2] >> 2) & 0x1F) | ((Gains[3] & 0x07) << 5) );
			pkt[14] = (uint8_t) ( ((Gains[3] >> 3) & 0x0F) | ((Gains[4] & 0x0F) << 4) );
		}

		private: void finalizePacket(uint8_t *pkt)
		{
			uint8_t expecting;
			string alert;
			expecting = (uint8_t)((int)pkt[16]+1);

			if (SequenceCounter != expecting)
			{
				TxNack++;
				//cout << (unsigned int) SequenceCounter << endl;
				//cout << (unsigned int) expecting << endl;
				alert = "Out of sequence packet, got " + to_string(unsigned(SequenceCounter)) + ", expecting " + to_string(unsigned(expecting)) + ".";
			//	cout << alert << endl;
				// if (this.EnableLogging)
				// 	kss.Log("ERR", alert);
			}

			pkt[16] = (uint8_t) SequenceCounter;

			fillInKraftChecksum(pkt);

		}

		private: static void fillInKraftChecksum(uint8_t *pkt)
		{
			int cksum = 0;
			pkt[17]=0;
			for (int i=0;i<17;i++)
				cksum += pkt[i];
			pkt[17] = (uint8_t) cksum;
		}

		public: void readCommandFromPkt(uint8_t *pkt)
		{
			Command[0] = pkt[1] +  (pkt[2] & 0x0F) * 256; // // azimuth 12 bits
			Command[1] = pkt[3] + (pkt[2] & 0xF0) * 16; // // elevator 12 bits
			Command[2] = pkt[4] + (pkt[5] & 0x0F) * 256; // elbow command 12 bits
			Command[3] = pkt[6] + (pkt[5] & 0xF0) * 16; // pitch command 12 bits
			Command[4] = pkt[7] + (pkt[8] & 0x0F) * 256; // yaw 12 bits
			Command[5] = pkt[9]; // wrist rotate 8 bits.
			Command[6] = pkt[10]; // grip 8 bits
		}

		public: void readFeedbackFromPkt(uint8_t *pkt)
		{
			Feedback[0] = pkt[1] + (pkt[2] & 0x0E) * 128; // wrist 11 bits
			Feedback[1] = pkt[3] + (pkt[2] & 0xE0) * 8;   // elevator 11 bits
			Feedback[2] = pkt[4] + (pkt[5] & 0x0E) * 128; // elbow position 11 bits
			Feedback[3] = pkt[6] + (pkt[5] & 0xE0) * 8;   // wrist pitch 11 bits
			Feedback[4] = pkt[7] + (pkt[8] & 0x0E) * 128; // wrist yaw 11 bits
			Feedback[5] = pkt[9] + (pkt[8] & 0xE0) * 8;   // wrist rotate 11 bits.
			// NOTE NO GRIP FEEDBACK !!!

			// Populate joint angles
			potsToAngles();
		}

		public: static string prettyPrintPacket ( uint8_t *ind )
		{
			string str = "";
			for ( unsigned int i=0; i<strlen((char*)ind); i++)
			{
				str += printf("%02X", ind[i]);
				str += " ";
			}
			// return str.trim(null);
			// remove that very last space for neatness
			return str;
		}

		private: void potsToAngles()
		{
			double current_angles_deg[7] = {0, 0, 0, 0, 0, 0, 0};
			for (int i = 0; i<6; i++)
			{
				current_angles_deg[i] = (2*Feedback[i] - CommandZero[i])*angleRange(i)/commandSpan(i);
				current_angles_rad[i] = (current_angles_deg[i] * M_PI)/180;
			}
			if (gripper_open == 0)
				current_angles_rad[6] = (MinAngle[6] * M_PI)/180;
			else
				current_angles_rad[6] = (MaxAngle[6] * M_PI)/180;
		}

		// TODO: support degrees or radians
		public: void getJointAngles(std::vector<double> &angles)
		{
			angles = current_angles_rad;
		}

		public: void getJointAngle(double &angle, int joint_id, bool degree = false)
		{
			if (degree)
				angle = current_angles_rad[joint_id] * 180/M_PI;
			else
				angle = current_angles_rad[joint_id];
		}

		// TODO: target angles are in degrees
		public: void getTargetAngle(double &angle, int joint_id, bool degree = false)
		{
			if (degree)
				angle = target_angles_deg[joint_id];
			else
				angle = target_angles_deg[joint_id] * M_PI/180;
		}

		public: void updateTargetPots(std::vector<double> &angle_rad)
		{
			angle_rad[5] = normalizeAnglePositive(angle_rad[5]);
			for (int i =0; i<6; i++)
			{
				target_angles_deg[i] = angle_rad[i]*180/M_PI;
			}

			angleToPots();
		}

		public: void setGripperCommand(int value)
		{
			// Limit range of command to 0->255
			if (value < 0)
				value = 0;
			else if (value > 255)
				value = 255;

			// Set command
			Command.at(6) = value;

			// // Gripper open or closed
			// gripper_open = value;
			// if (value == 0)
			// 	Command.at(6) = 255; // closed
			// else
			// 	Command.at(6) = 0; // open
		}

		public: void setCommandFromPID(double PIDCommand, int joint_id)
		{
			// Position controlled joints
			if (joint_id < 5)
				Command.at(joint_id) = 2*Feedback.at(joint_id) + PIDCommand;
			else if (joint_id == 5)
			{
				double cmd = PIDCommand;

				// Check if wrist roll is off by more than 10 degrees
				double roll_error = shortestAngularDistance(current_angles_rad[5], target_angles_deg[5]*M_PI/180.0);
				if (std::abs(roll_error) > 10.0*M_PI/180.0) 
					// Only move wrist roll in clockwise direction
					cmd = -std::abs(PIDCommand);
				
				// Wrist roll velocity controlled
				// PID command is reduced in magnitude to support ROS PID gain limits
				Command.at(5) = ZeroVel[0] + cmd*100.0;
			}
			enforceCommandLimits();
		}

		public: void setAllCommandsFromPID(std::vector<double> PIDCommand)
		{
			// Position controlled joints
			for (int i=0;i<5; i++)
				Command.at(i) = 2*Feedback.at(i) + PIDCommand.at(i);

			// Wrist roll velocity controlled
			Command.at(5) = ZeroVel[0] + PIDCommand.at(5);

			enforceCommandLimits();
		}

		public: void angleToPots()
		{
			for (int i=0; i<6; i++)
			{
				TargetPots[i] = CommandZero[i] + (commandSpan(i)*target_angles_deg[i])/angleRange(i);
			}
			// TargetPots[0] += (commandSpan(0)*SlewOffset)/angleRange(0);
			enforcePotsLimits();

			// Command[6] = 0;
		}

		public: int potLimitMax(int axis)
		{
			return CommandZero[axis] + MaxAngle[axis]*CommandTicksPerDegree[axis];
		}

		public: int potLimitMin(int axis)
		{
			return CommandZero[axis] + MinAngle[axis]*CommandTicksPerDegree[axis];
		}

		public: void enforcePotsLimits()
		{
			for (int i=0; i<6; i++)  //for (int i=0; i<5; i++)
			{
				if (TargetPots[i]>potLimitMax(i))
				 	TargetPots[i] = potLimitMax(i);
				else if (TargetPots[i]<potLimitMin(i))
					TargetPots[i] = potLimitMin(i);
			}
		}

		public: void enforceCommandLimits()
		{
			// Wrist is continuous rotation and velocity controlled, so don't enforce joint limits.
			// Velocity limits are enforced by the external PID controller
			for (int i=0; i<5; i++)
			{
				if (Command[i]>potLimitMax(i))
				 	Command[i] = potLimitMax(i);
				else if (Command[i]<potLimitMin(i))
					Command[i] = potLimitMin(i);
				// if (Command[i]>CommandMaximum[i])
				 	// Command[i] = CommandMaximum[i];
				// else if (Command[i]<CommandMin[i])
					// Command[i] = CommandMin[i];
			}
			// Command[5] = ZeroVel[0]+30;
		}

		// copied from chomp_utils.h
   		public: static inline double normalizeAnglePositive(double angle)
   		{
   		  return fmod(fmod(angle, 2.0 * M_PI) + 2.0 * M_PI, 2.0 * M_PI);
   		}

		// copied from chomp_utils.h
   		public: static inline double normalizeAngle(double angle)
   		{
   		  double a = normalizeAnglePositive(angle);
   		  if (a > M_PI)
   		    a -= 2.0 * M_PI;
   		  return a;
   		}

		// copied from chomp_utils.h
   		public: static inline double shortestAngularDistance(double start, double end)
   		{
   		  double res = normalizeAnglePositive(normalizeAnglePositive(end) - normalizeAnglePositive(start));
   		  if (res > M_PI)
   		  {
   		    res = -(2.0 * M_PI - res);
   		  }
   		  return normalizeAngle(res);
   		}

		// public: void PositionToVelocityPID()
		public: void wristPID()
		{
			int i = 5;
			int j = 0;

			error_pos[j] = shortestAngularDistance(current_angles_rad[i], target_angles_deg[i]*M_PI/180.0); // Continous joint so use shortest angular distance
			error_pos[j] = error_pos[j]*180.0/M_PI; // convert to degrees for easier PID tuning

			error_der[j] = (error_pos[j] - error_prev[j])*masterRate;///dt;

			if (hydraulicsEnabled)
				error_int[j] = error_int[j] + error_pos[j];

			if (error_int[j]>ki_limit[j])
				error_int[j] =  ki_limit[j];
			else
				if (error_int[j]<-ki_limit[j])
						error_int[j] = - ki_limit[j];

			float pid_vel = 0;

			// squared response on error_pose
			// pid_vel = (int)(Kp[j]*error_pos[j]*abs(error_pos[j]) + Kd[j]*error_der[j] + Ki[j]*error_int[j]);
			// linear response on error_pos
			pid_vel = (Kp[j]*error_pos[j] + Kd[j]*error_der[j] + Ki[j]*error_int[j]);

			if (error_pos[j] > 0 && pid_vel < 0)
				pid_vel = 0;
			else
				if (error_pos[j] < 0 && pid_vel > 0)
					pid_vel = 0;

			// Bang-Bang controller
			// Works but wrist stutters
			// if (error_pos[j] > 3)
			// 	vel[j] = ZeroVel[j] + 100;
			// else if (error_pos[j] < -3)
			// 	vel[j] = ZeroVel[j] - 100;
			// else
			// 	vel[j] = ZeroVel[j];

			// PID controller with deadzone
			// if (error_pos[j] > 3 || error_pos[j] < -3)
			if (error_pos[j] > 10 || error_pos[j] < -10)
				vel[j] = ZeroVel[j] + (int) pid_vel;
			else
				vel[j] = ZeroVel[j];

			// Check limits
			if (vel[j] > VelMax[j])
				vel[j] = VelMax[j];
			else
				if (vel[j] < VelMin[j])
					vel[j] = VelMin[j];

			error_prev[j] = error_pos[j];
		}

		public: void getJointPotsFeedback(std::vector<double> &potsfb)
		{
			for (unsigned int i=0; i<Feedback.size(); i++)
				potsfb.at(i) = Feedback.at(i);
		}

		public: void getJointPotsTarget(std::vector<double> &potstg)
		{
			for (unsigned int i=0; i<TargetPots.size(); i++)
				potstg.at(i) = TargetPots.at(i);
		}

		public: void getJointPotsCommands(std::vector<double> &potscmd)
		{
			for (unsigned int i=0; i<Command.size(); i++)
				potscmd.at(i) = Command.at(i);
		}

		public: void closedHydraulics()
		{
			setHydraulicsEnabled(false);
			//UdpTx(angle_rad);
			for (int i=0;i<400;i++) {
				sendIdle();
				UdpRx();
				usleep(10000);
			}
		}
	};
}

#endif
