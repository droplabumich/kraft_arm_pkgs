/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2020, University of Michigan, Ann Arbor
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Univ of MI, Ann Arbor nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Gideon Billings
   Desc:   ros_control hardware interface for the Kraft manipulator
           For a more detailed simulation example, see sim_hw_interface.cpp
*/

#include <kraft_control/kraft_hw_interface_new.h>

namespace kraft_control
{

KraftHWInterface::KraftHWInterface(ros::NodeHandle &nh, urdf::Model *urdf_model)
  : ros_control_boilerplate::GenericHWInterface(nh, urdf_model),
    pid_commands(NUM_AXES), pid_setpoint_pub(NUM_AXES-1), pid_feedback_pub(NUM_AXES-1), pid_command_sub(NUM_AXES-1)
{
  robot=new ROSKraft::Kraft(nh);
  ROS_INFO_NAMED("kraft_hw_interface", "KraftHWInterface Ready.");
  joint_cmds_pub = nh_.advertise<kraft_ros_control_boilerplate::Float64MultiArrayStamped>(CMD_TOPIC,1);
  joint_fbk_pub = nh_.advertise<kraft_ros_control_boilerplate::Float64MultiArrayStamped>(FBK_TOPIC,1);
  gripper_open_close = nh_.subscribe<std_msgs::UInt8>("/kraft_arm/gripper", 1, &KraftHWInterface::gripperCallback, this);

  for (unsigned int i=0; i<pid_setpoint_pub.size(); i++) {
    pid_setpoint_pub.at(i) = nh_.advertise<std_msgs::Float64>("/kraft_arm/pid_setpoint_" + std::to_string(i), 1);
    pid_feedback_pub.at(i) = nh_.advertise<std_msgs::Float64>("/kraft_arm/pid_feedback_" + std::to_string(i), 1);
    pid_command_sub.at(i) = nh_.subscribe<std_msgs::Float64>("/kraft_arm/pid_command_" + std::to_string(i), 1, boost::bind(&KraftHWInterface::pidCommandCallback, this, _1, i));
  }

  connect_service = nh_.advertiseService("connect", &KraftHWInterface::connect, this);
  disconnect_service = nh_.advertiseService("disconnect", &KraftHWInterface::disconnect, this);
}

bool KraftHWInterface::connect(kraft_ros_control_boilerplate::Connect::Request &req, kraft_ros_control_boilerplate::Connect::Response &res)
{
  // return robot->connect();
  return true;
}

bool KraftHWInterface::disconnect(kraft_ros_control_boilerplate::Disconnect::Request &req, kraft_ros_control_boilerplate::Disconnect::Response &res)
{
  // return robot->disconnect();
  return true;
}

void KraftHWInterface::pidCommandCallback(const std_msgs::Float64::ConstPtr &msg, int joint_id)
{
  // Mutex not needed here because each joint callback only accesses one vector element with no overlap
  // Currently no guarantee that all controllers update before command transmission to manipulator.
  // pid_commands.at(joint_id) = msg.data;
  robot->setCommandFromPID(msg->data, joint_id);
}

void KraftHWInterface::gripperCallback(const std_msgs::UInt8 data)
{
    int gripper_command = data.data;
    robot->setGripperCommand(gripper_command);
    // std::cout<<"Setting Gripper State: "<< gripper_command << std::endl;
    // std::cout.flush();
}

void KraftHWInterface::send_cmd_pots(const std::vector<double> &potscmd)
{
   kraft_ros_control_boilerplate::Float64MultiArrayStamped msg;
   msg.header.stamp = ros::Time::now();
   msg.array.data = potscmd;
   joint_cmds_pub.publish(msg);
}

void KraftHWInterface::send_fbk_pots(const std::vector<double> &potsfbk)
{
   kraft_ros_control_boilerplate::Float64MultiArrayStamped msg;
   msg.header.stamp = ros::Time::now();
   msg.array.data = potsfbk;
   joint_fbk_pub.publish(msg);
}

void KraftHWInterface::sendPIDPots()
{
  std::vector<double> feedback(7);
  std::vector<double> target(7);

  robot->getJointPotsFeedback(feedback);
  robot->getJointPotsTarget(target);

  // feedback = std::vector<double>{986, 1314, 681, 968, 1000, 833, 0};

  // for (unsigned int i=0; i<pid_setpoint_pub.size(); i++) {
  for (unsigned int i=0; i<5; i++) {
    std_msgs::Float64 fb, tg;
    fb.data = feedback[i]*2; // Kraft feedback is half value of commands
    tg.data = target[i];
    pid_feedback_pub[i].publish(fb);
    pid_setpoint_pub[i].publish(tg);
  }

  // wrist joint is special case
  std_msgs::Float64 fb, tg;
  double curr_wrist_angle, targ_wrist_angle;
  robot->getJointAngle(curr_wrist_angle, 5, true);
  robot->getTargetAngle(targ_wrist_angle, 5, true);
  fb.data = curr_wrist_angle;
  tg.data = targ_wrist_angle;
  pid_feedback_pub[5].publish(fb);
  pid_setpoint_pub[5].publish(tg);
}

void KraftHWInterface::read(ros::Duration &elapsed_time)
{
  if (robot->connected())
  {
    std::vector<double> potsfb(7);

    // robot->receive();
    robot->UdpRx();

    robot->getJointAngles(joint_position_);

    robot->getJointPotsFeedback(potsfb);
    send_fbk_pots(potsfb);

    // update target pots
    robot->updateTargetPots(joint_position_command_);
    // std::vector<double> tmp{0.523599, 0.523599, 0.523599, 0.523599, 0.523599, 0.523599, 0};
    // robot->updateTargetPots(tmp);

    // update pid
    sendPIDPots();
  }
}

void KraftHWInterface::write(ros::Duration &elapsed_time)
{
  if (robot->connected())
  {
    // Safety
    enforceLimits(elapsed_time);

    // ----------------------------------------------------
    // ----------------------------------------------------
    // ----------------------------------------------------
    //
    // FILL IN YOUR WRITE COMMAND TO USB/ETHERNET/ETHERCAT/SERIAL ETC HERE
    //
    // FOR A EASY SIMULATION EXAMPLE, OR FOR CODE TO CALCULATE
    // VELOCITY FROM POSITION WITH SMOOTHING, SEE
    // sim_hw_interface.cpp IN THIS PACKAGE
    //
    // DUMMY PASS-THROUGH CODE
    // for (std::size_t joint_id = 0; joint_id < 7; ++joint_id)
    //   joint_position_[joint_id] = joint_position_command_[joint_id];
    // END DUMMY CODE
    //
    // ----------------------------------------------------
    // ----------------------------------------------------
    // ----------------------------------------------------

    std::vector<double> potscmd(7);
    // Joint commands now updated in PID callback
    // robot->setAllCommandsFromPID(pid_commands);
    // robot->send();
    robot->UdpTx();
    robot->getJointPotsCommands(potscmd);
    send_cmd_pots(potscmd);
  }
}

void KraftHWInterface::enforceLimits(ros::Duration &period)
{
  // ----------------------------------------------------
  // ----------------------------------------------------
  // ----------------------------------------------------
  //
  // CHOOSE THE TYPE OF JOINT LIMITS INTERFACE YOU WANT TO USE
  // YOU SHOULD ONLY NEED TO USE ONE SATURATION INTERFACE,
  // DEPENDING ON YOUR CONreceivedROL METHOD
  //
  // EXAMPLES:
  //
  // Saturation Limits ---------------------------
  //
  // Enforces position and velocity
  pos_jnt_sat_interface_.enforceLimits(period);
  //
  // Enforces velocity and acceleration limits
  // vel_jnt_sat_interface_.enforceLimits(period);
  //
  // Enforces position, velocity, and effort
  // eff_jnt_sat_interface_.enforceLimits(period);

  // Soft limits ---------------------------------
  //
  // pos_jnt_soft_limits_.enforceLimits(period);
  // vel_jnt_soft_limits_.enforceLimits(period);
  // eff_jnt_soft_limits_.enforceLimits(period);
  //
  // ----------------------------------------------------
  // ----------------------------------------------------
  // ----------------------------------------------------
}

}  // namespace
