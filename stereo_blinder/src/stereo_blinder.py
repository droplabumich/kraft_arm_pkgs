#!/usr/bin/env python
import roslib
import rospy
import rospkg
from urdf_parser_py.urdf import URDF
import tf2_ros

from sensor_msgs.msg import Image as RosImage

from PIL import Image as PILImage
from PIL import ImageEnhance as PILImageEnhance
import numpy as np
import argparse
import transforms3d as tf3d
import json
import sys
from os import path as osp
from time import time
import datetime
from cv_bridge import CvBridge, CvBridgeError
import cv2

import viewer as v
import glfw # lean window system wrapper for OpenGL


def fullpath(relative_path):
    prefix = 'package://'
    rospack = rospkg.RosPack()
    if relative_path.startswith(prefix):
        relative_path = relative_path[len(prefix):]
        path_list = relative_path.split('/')
        pkg_path = rospack.get_path(path_list[0])
        path = osp.join(pkg_path, *path_list[1:])
    else:
        path = relative_path
    return path


def parseModels(robot):
    models = []
    for link in robot.links:
        model = {}
        model['name'] = link.name
        model['path'] = fullpath(link.visuals[0].geometry.filename)
        models.append(model)
    return models


class StereoBlinder():

    def __init__(self, name, urdf_path, camera_path):
        self.name = name # node name

        self.timestamp = 0
        self.bridge = CvBridge()

        robot = URDF.from_xml_file(urdf_path)
        camera = v.Camera(camera_path)
        self.viewer = v.Viewer(camera)
        self.models = parseModels(robot)
        for model in self.models:
            print(model['name'])
            self.viewer.add(*[mesh for mesh in v.load(model['path'], model['name'])])

        self.tfBuffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tfBuffer)

        rospy.Subscriber("/camera/left/image_raw", RosImage, self.camera_callback, queue_size=100)
        self.pub = rospy.Publisher("/camera/left/image_raw_masked", RosImage, queue_size=100)

        self.update = False
        self.msg = True

    def mask_image(self):
        self.timestamp = self.msg.header.stamp #- #rospy.Time.from_sec(2)

        # bits = 16  ### fixme
        bits = 8

        try:
            if bits == 16:
                cv2_img = self.bridge.imgmsg_to_cv2(self.msg,"passthrough")
                cv2_img = np.array(cv2_img, dtype=np.uint16)
                cv2_img = cv2_img*16
                cv2_img = cv2.cvtColor(cv2_img,cv2.COLOR_BAYER_RG2RGB)
                rospy.loginfo("Successfully converted")
            elif bits == 8:
                # Convert the raw image to a version python can work with
                cv2_img = self.bridge.imgmsg_to_cv2(self.msg, "bgr8").copy()
                rospy.loginfo("Successfully converted")
            else:
                raise Exception("Expected 8 or 16 bit images. Cannot decode " + str(bits) + " bits")
        except CvBridgeError as e:
            rospy.logerr("Error converting Error, %s", e)
            print e
        else:
            rospy.loginfo("Adjusting image and publishing")
            cv2_img = cv2.cvtColor(cv2_img,cv2.COLOR_BGR2RGB)

            # pil_img = PILImage.fromarray(cv2_img)
            # cv2_img = np.array(pil_img)
            # cv2_img = cv2.cvtColor(cv2_img,cv2.COLOR_RGB2BGR)

            # msg_img = self.bridge.cv2_to_imgmsg(cv2_img, encoding="bgr8")
            # msg_img.header.stamp = msg.header.stamp
            # try:
            #   self.pub.publish(msg_img)
            # except CvBridgeError as e:
            #   print(e)

        tf_dict = {}
        for model in self.models:
            try:
                transform = self.tfBuffer.lookup_transform('camera_left', model['name'], rospy.Time(0))
                print(transform)
                tf_dict[model['name']] = transform
            except Exception as e:
                print(str(e))
                tf_dict[model['name']] = None
                continue
        # image = images[IMAGE_ID_LIST[INDEX]]
        # self.viewer.background.set(pil_img)
        # viewer.clear_bboxes()
        # viewer.clear_markers()
        for model, tf in tf_dict.items():
            if tf is None:
                print("continued")
                continue
            name = model
            self.viewer.set_active(name)
            Rt = np.eye(4)
            Rt[0:3,3] = np.array([tf.transform.translation.x, tf.transform.translation.y, tf.transform.translation.z])
            quat = np.array([tf.transform.rotation.w, tf.transform.rotation.x, tf.transform.rotation.y, tf.transform.rotation.z])
            Rt[0:3,0:3] = tf3d.quaternions.quat2mat(quat)
            # Rt[2,3] += 1
            self.viewer.set_pose_matrix(Rt)
        self.viewer.render()

        mask = self.viewer.read_image()
        # mask = cv2.bitwise_not(cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)) # For black mask
        mask = cv2.resize(mask, (cv2_img.shape[1], cv2_img.shape[0]), interpolation=cv2.INTER_NEAREST)

        # f_img = cv2.bitwise_and(cv2_img, cv2_img, mask=mask) # For black mask
        f_img = cv2.addWeighted(mask, 1, cv2_img, 1, 0, cv2_img) # For white mask
        f_img = cv2.cvtColor(f_img,cv2.COLOR_RGB2BGR)

        msg_img = self.bridge.cv2_to_imgmsg(f_img, encoding="bgr8")
        msg_img.header.stamp = self.msg.header.stamp
        try:
          self.pub.publish(msg_img)
        except CvBridgeError as e:
          print(e)

        # glfw.poll_events()
        self.update = False


    def camera_callback(self, msg):
        self.msg = msg
        self.update = True



if __name__ == "__main__":
    rospy.init_node('stereo_blinder')
    glfw.init()

    urdf_path = fullpath('package://stereo_blinder/urdf/kraft.urdf')
    camera_path = fullpath('package://stereo_blinder/cfg/kraft_stereo_left.yaml')

    blinder = StereoBlinder(rospy.get_name(), urdf_path, camera_path)
    rate = rospy.Rate(15.0)
    while not rospy.is_shutdown() and not glfw.window_should_close(blinder.viewer.win):
        if blinder.update:
            blinder.mask_image()
        try:
            rate.sleep()
        except Exception as e:
            print(e)

    # robot = URDF.from_xml_file(urdf_path)

    # camera = v.Camera(camera_path)
    # viewer = v.Viewer(camera)
    # models = parseModels(robot)
    # for model in models:
    #     print(model['name'])
    #     viewer.add(*[mesh for mesh in v.load(model['path'], model['name'])])


    # tfBuffer = tf2_ros.Buffer()
    # listener = tf2_ros.TransformListener(tfBuffer)

    # rate = rospy.Rate(10.0)
    # while not rospy.is_shutdown() and not glfw.window_should_close(viewer.win):
    #     tf_dict = {}
    #     for model in models:
    #         try:
    #             transform = tfBuffer.lookup_transform('Camera', model['name'], rospy.Time())
    #             tf_dict[model['name']] = transform
    #         except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
    #             tf_dict[model['name']] = None
    #             continue
    #     # image = images[IMAGE_ID_LIST[INDEX]]
    #     # viewer.background.set(osp.join(args.images, image['file_name']))
    #     # viewer.clear_bboxes()
    #     # viewer.clear_markers()
    #     for model, tf in tf_dict.items():
    #         if tf is None:
    #             continue
    #         name = model
    #         viewer.set_active(name)
    #         Rt = np.eye(4)
    #         Rt[0:3,3] = np.array([tf.transform.translation.x, tf.transform.translation.y, tf.transform.translation.z])
    #         quat = np.array([tf.transform.rotation.w, tf.transform.rotation.x, tf.transform.rotation.y, tf.transform.rotation.z])
    #         Rt[0:3,0:3] = tf3d.quaternions.quat2mat(quat)
    #         # Rt[2,3] += 1
    #         viewer.set_pose_matrix(Rt)
    #     viewer.render()
    #     glfw.poll_events()
    #     rate.sleep()
