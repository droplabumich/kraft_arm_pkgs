#!/usr/bin/env python3

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QObject, pyqtSlot, QTimer
from mainwindow import Ui_MainWindow

import roslib
import rospy
import rospkg
from geometry_msgs.msg import TransformStamped
from moveit_msgs.msg import RobotTrajectory, DisplayTrajectory
from std_msgs.msg import Bool, UInt8, String, Float32
from pick_and_place import PickAndPlace

from controller_manager_msgs.srv import LoadController, SwitchController
from kraft_ros_control_boilerplate.srv import Connect, Disconnect

import sys, os
from os import path as osp
import numpy as np
import argparse
from threading import Thread

from nlu_msgs.msg import GroundedAction

class MainWindowUIClass( Ui_MainWindow ):
    def __init__(self, main_window):
        super(MainWindowUIClass, self).__init__()

        # Init gui items
        self.setupUi(main_window)

        # Setup timed loop
        self.timer = QTimer()
        self.timer.timeout.connect(self.refreshAll)
        self.timer.start(3)

        # Init ros
        rospy.init_node('rov_autonomy_master')
        # self.rate = rospy.Rate(500.0)

        self.planning = PickAndPlace()
        self.planning.clear_world()
        self.planning.update_objects()
        # self.planning.fake_object_generator() ## Demo

        self.plan = None
        self.success = False
        self.frozen = False
        self.last_plan_key = None
        self.last_request_key = None
        self.command_key = None

         # Should be False. Only used for development on MacOS via Docker
        self.devel = False

        self.command = {
            'constraint': None,
            'object': None,
            'reference_relation': None
        }
        self.last_command = self.command

        self.users = {}

        self.fillObjectTypeList()

        try:
            rospy.wait_for_service('/kraft/controller_manager/load_controller', 3)
            rospy.wait_for_service('/kraft/controller_manager/switch_controller', 3)
        except:
            pass
        self.load_controller = rospy.ServiceProxy('/kraft/controller_manager/load_controller', LoadController)
        self.switch_controller = rospy.ServiceProxy('/kraft/controller_manager/switch_controller', SwitchController)

        try:
            rospy.wait_for_service('/kraft/connect', 3)
            rospy.wait_for_service('/kraft/disconnect', 3)
            self.connect_arm = rospy.ServiceProxy('/kraft/connect', Connect)
            self.disconnect_arm = rospy.ServiceProxy('/kraft/disconnect', Disconnect)
        except Exception as e:
            print("Kraft arm service calls not available! Assuming sim mode: %s"%e)

        self.ros_thread = Thread(target = self.planning.loop)
        self.ros_thread.start()

        # Moveit status
        rospy.Subscriber("/kraft/pick_and_place_status", String, self.moveit_status_callback)

        # Remote unity interface subscribers
        rospy.Subscriber("/unity_interface/request_plan", TransformStamped, self.unity_plan_req_callback)
        rospy.Subscriber("/unity_interface/request_tool_plan", String, self.unity_tool_req_callback)
        rospy.Subscriber("/unity_interface/command_plan", String, self.unity_command_callback) # TODO: make this a custom message with user ID
        rospy.Subscriber("/unity_interface/gripper_command", UInt8, self.unity_gripper_cmd_callback)
        rospy.Subscriber("/unity_interface/connected_scientists", String, self.unity_users_callback)

        # Remote unity interface publishers
        self.active_scientist_pub = rospy.Publisher("/unity_interface/active_scientist", String, queue_size=1)
        self.activeUserChangeSlot()

        self.active_topics_pub = rospy.Publisher("/unity_interface/active_topics", String, queue_size=1)
        self.activeTopicsChangeSlot()

        self.override_pub = rospy.Publisher("/unity_interface/remote_override", Bool, queue_size=1)
        self.override = False
        self.overrideChangeSlot()

        self.status_pub = rospy.Publisher("/unity_interface/status", String, queue_size=5)
        self.camera_fps_pub = rospy.Publisher("/unity_interface/camera_fps", Float32, queue_size=5)
        self.trajectory_pub = rospy.Publisher("/unity_interface/trajectory", DisplayTrajectory, queue_size=1)

        # rospy.Subscriber("/grounded_action", GroundedAction, self.groundedActionCallback)

        self.ros_subscribe_thread = Thread(target = self.ros_subscribe_loop)
        self.ros_subscribe_thread.start()



    def __del__(self):
        self.ros_thread.join()
        self.ros_subscribe_thread.join()
        # pass

    def setupUi( self, MW ):
        ''' Setup the UI of the super class, and add here code
        that relates to the way we want our UI to operate.
        '''
        super(MainWindowUIClass, self).setupUi( MW )
        self.radioButton.setChecked(True)
        # Connect slots
        ck_box_list = [self.checkBox, self.checkBox_2, self.checkBox_3, self.checkBox_4]
        for box in ck_box_list:
            box.setChecked(True)
            box.stateChanged.connect(self.activeTopicsChangeSlot)
        self.checkBox_5.stateChanged.connect(self.overrideChangeSlot)
        self.radioButton.toggled.connect(self.controlModeChangeSlot)
        self.radioButton_2.toggled.connect(self.controlModeChangeSlot)
        self.freeze.clicked.connect(self.freezeTargetSlot)
        self.comboBox.currentIndexChanged.connect(self.targetSelectionChangeSlot)
        self.comboBox_2.currentIndexChanged.connect(self.objectTypeChangeSlot)
        self.comboBox_3.currentIndexChanged.connect(self.activeUserChangeSlot)
        self.comboBox_4.currentIndexChanged.connect(self.cameraFpsChangeSlot)
        self.stateMachine_1.clicked.connect(self.goToPreGraspPoseSlot)
        self.stateMachine_2.clicked.connect(self.goToGraspPoseSlot)
        self.stateMachine_3.clicked.connect(self.executeGraspSlot)
        self.stateMachine_4.clicked.connect(self.removeToolFromTraySlot)
        self.stateMachine_5.clicked.connect(self.goToSampleLocationSlot)
        self.stateMachine_6.clicked.connect(self.takeSampleSlot)
        self.stateMachine_7.clicked.connect(self.extractSampleSlot)
        self.stateMachine_8.clicked.connect(self.returnToPreGraspPoseSlot)
        self.stateMachine_9.clicked.connect(self.returnToGraspPoseSlot)
        self.stateMachine_10.clicked.connect(self.releaseGraspSlot)
        self.stateMachine_11.clicked.connect(self.retreatSlot)
        self.stateMachine_12.clicked.connect(self.goToGraspPoseNotchSlot)
        self.stow.clicked.connect(self.stowSlot)
        self.stop.clicked.connect(self.stopSlot)
        self.ready.clicked.connect(self.readySlot)
        self.plan.clicked.connect(self.showPlanSlot)
        self.execute.clicked.connect(self.executeSlot)
        self.openGrip.clicked.connect(self.openGripSlot)
        self.closeGrip.clicked.connect(self.closeGripSlot)
        self.stateMachineIndicators = [self.indicator_1, self.indicator_2, self.indicator_3, self.indicator_4, self.indicator_5,
            self.indicator_6, self.indicator_7, self.indicator_8, self.indicator_9, self.indicator_10, self.indicator_11,
            self.indicator_stow, self.indicator_ready, self.indicator_stop, self.indicator_12]
        self.remotePlanIndicator = self.indicator_13
        self.remoteExecuteIndicator = self.indicator_14
        self.activePlanIndicator = self.indicator_15
        self.remotePlanIndicator.setEnabled(False)
        self.remoteExecuteIndicator.setEnabled(False)
        self.activePlanIndicator.setEnabled(False)
        for le in self.stateMachineIndicators:
            le.setEnabled(False)
        self.comboBox_2.setEditable(False)
        self.connectButton.clicked.connect(self.connectArmSlot)
        self.disconnectButton.clicked.connect(self.disconnectArmSlot)
        self.connectIndicator.setStyleSheet("background-color: rgb(255, 0, 0);")

    def fillObjectTypeList(self):
        all_items = [self.comboBox_2.itemText(i) for i in range(self.comboBox_2.count())]
        for key in self.planning.object_types():
            if key not in all_items:
                self.comboBox_2.addItem(key)

    def clearObjectTypeList(self):
        all_items = [self.comboBox_2.itemText(i) for i in range(self.comboBox_2.count())]
        for i, key in enumerate(all_items):
            self.comboBox_2.removeItem(i)

    def updateObjectTypeList(self):
        if self.comboBox.count() == 0:
            return
        else:
            name = str(self.comboBox.currentText())
            key = self.planning.object_type(name)
            self.comboBox_2.setCurrentText(key)

    def updateObjectList(self):
        all_items = [str(self.comboBox.itemText(i)) for i in range(self.comboBox.count())]
        for i, name in enumerate(all_items):
            if name not in self.planning.object_names():
                self.comboBox.removeItem(i)
        for name in self.planning.object_names():
            if name not in all_items:
                self.comboBox.addItem(name)

    def refreshAll( self ):
        # self.planning.update_objects()
        self.updateObjectList()
        self.updateObjectTypeList()
        if self.plan:
            self.activePlanIndicator.setStyleSheet("background-color: rgb(0, 255, 0);")
        else:
            self.activePlanIndicator.setStyleSheet("background-color: rgb(255, 0, 0);")
        # self.horizontalSlider.setValue(self.image_index)

    def clearRemoteIndicators(self):
        self.last_plan_key = None
        self.last_request_key = None
        self.command_key = None
        self.remoteExecuteIndicator.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.remotePlanIndicator.setStyleSheet("background-color: rgb(255, 255, 255);")

    def rosSlot(self):
        # if not rospy.is_shutdown():
            # self.rate.sleep()
        # pass
        # self.planning.run_once()
        self.refreshAll()

    def freezeTargetSlot(self):
        name = str(self.comboBox.currentText())
        if name is not None:
            self.frozen = not self.frozen
            if self.frozen:
                self.freeze.setText("Unreeze Target")
                self.planning.freeze_pose = name
            else:
                self.freeze.setText("Freeze Target")
                self.planning.freeze_pose = None
        self.refreshAll()

    def targetSelectionChangeSlot(self):
        self.frozen = False
        self.freeze.setText("Freeze Target")
        self.planning.freeze_pose = None
        name = str(self.comboBox.currentText())
        self.planning.update_sample_model(name)
        self.refreshAll()

    def objectTypeChangeSlot(self):
        name = str(self.comboBox.currentText())
        key = str(self.comboBox_2.currentText())
        # if name != '':
            # self.planning.set_object_type(name, key)
        self.refreshAll()

    def closeGripSlot(self):
        self.clearRemoteIndicators()
        self.planning.close_gripper()
        self.refreshAll()

    def openGripSlot(self):
        self.clearRemoteIndicators()
        self.planning.open_gripper()
        self.refreshAll()

    def showPlanSlot(self):
        if self.plan is not None:
            self.planning.display_plan(self.plan)
        self.refreshAll()

    def executeSlot(self):
        self.clearRemoteIndicators()
        self.planning.execute_plan(self.plan, wait=False)
        self.refreshAll()

    def goToPreGraspPoseSlot(self):
        name = str(self.comboBox.currentText())
        if name != '':
            self.clearRemoteIndicators()
            if self.devel:
                self.success = True
                self.plan = None
            else:
                self.plan, self.success = self.planning.pre_grasp_approach(name)
            for le in self.stateMachineIndicators:
                le.setStyleSheet("background-color: rgb(255, 255, 255);")
            if self.success:
                self.stateMachineIndicators[0].setStyleSheet("background-color: rgb(0, 255, 0);")
            else:
                self.stateMachineIndicators[0].setStyleSheet("background-color: rgb(255, 0, 0);")
            self.refreshAll()

    def goToGraspPoseSlot(self):
        name = str(self.comboBox.currentText())
        if name != '':
            self.clearRemoteIndicators()
            if self.devel:
                self.success = True
                self.plan = None
            else:
                self.plan, self.success = self.planning.grasp_approach(name)
            for le in self.stateMachineIndicators:
                le.setStyleSheet("background-color: rgb(255, 255, 255);")
            if self.success:
                self.stateMachineIndicators[1].setStyleSheet("background-color: rgb(0, 255, 0);")
            else:
                self.stateMachineIndicators[1].setStyleSheet("background-color: rgb(255, 0, 0);")
            self.refreshAll()

    def goToGraspPoseNotchSlot(self):
        name = str(self.comboBox.currentText())
        if name != '':
            self.clearRemoteIndicators()
            if self.devel:
                self.success = True
                self.plan = None
            else:
                self.plan, self.success = self.planning.grasp_approach(name, notch=True)
            for le in self.stateMachineIndicators:
                le.setStyleSheet("background-color: rgb(255, 255, 255);")
            if self.success:
                self.stateMachineIndicators[14].setStyleSheet("background-color: rgb(0, 255, 0);")
            else:
                self.stateMachineIndicators[14].setStyleSheet("background-color: rgb(255, 0, 0);")
            self.refreshAll()

    def executeGraspSlot(self):
        name = str(self.comboBox.currentText())
        if name != '':
            self.clearRemoteIndicators()
            if self.devel:
                self.success = True
            else:
                self.success = self.planning.grasp_object(name)
            self.plan = None
            for le in self.stateMachineIndicators:
                le.setStyleSheet("background-color: rgb(255, 255, 255);")
            if self.success:
                self.stateMachineIndicators[2].setStyleSheet("background-color: rgb(0, 255, 0);")
            else:
                self.stateMachineIndicators[2].setStyleSheet("background-color: rgb(255, 0, 0);")
            self.refreshAll()

    def removeToolFromTraySlot(self):
        name = str(self.comboBox.currentText())
        if name != '':
            self.clearRemoteIndicators()
            if self.devel:
                self.success = True
                self.plan = None
            else:
                self.plan, self.success = self.planning.grasp_retreat(name)
            for le in self.stateMachineIndicators:
                le.setStyleSheet("background-color: rgb(255, 255, 255);")
            if self.success:
                self.stateMachineIndicators[3].setStyleSheet("background-color: rgb(0, 255, 0);")
            else:
                self.stateMachineIndicators[3].setStyleSheet("background-color: rgb(255, 0, 0);")
            self.refreshAll()

    def goToSampleLocationSlot(self):
        self.clearRemoteIndicators()
        if self.devel:
            self.success = True
            self.plan = None
        else:
            self.plan, self.success = self.planning.sample(approach=True)
        for le in self.stateMachineIndicators:
            le.setStyleSheet("background-color: rgb(255, 255, 255);")
        if self.success:
            self.stateMachineIndicators[4].setStyleSheet("background-color: rgb(0, 255, 0);")
        else:
            self.stateMachineIndicators[4].setStyleSheet("background-color: rgb(255, 0, 0);")
        self.refreshAll()

    def takeSampleSlot(self):
        self.clearRemoteIndicators()
        if self.devel:
            self.success = True
            self.plan = None
        else:
            self.plan, self.success = self.planning.sample(approach=False)
        for le in self.stateMachineIndicators:
            le.setStyleSheet("background-color: rgb(255, 255, 255);")
        if self.success:
            self.stateMachineIndicators[5].setStyleSheet("background-color: rgb(0, 255, 0);")
        else:
            self.stateMachineIndicators[5].setStyleSheet("background-color: rgb(255, 0, 0);")
        self.refreshAll()

    def extractSampleSlot(self):
        self.clearRemoteIndicators()
        if self.devel:
            self.success = True
            self.plan = None
        else:
            self.plan, self.success = self.planning.sample(approach=True)
        for le in self.stateMachineIndicators:
            le.setStyleSheet("background-color: rgb(255, 255, 255);")
        if self.success:
            self.stateMachineIndicators[6].setStyleSheet("background-color: rgb(0, 255, 0);")
        else:
            self.stateMachineIndicators[6].setStyleSheet("background-color: rgb(255, 0, 0);")
        self.refreshAll()

    def returnToPreGraspPoseSlot(self):
        self.clearRemoteIndicators()
        if self.devel:
            self.success = True
            self.plan = None
        else:
            self.plan, self.success = self.planning.pre_grasp_return()
        for le in self.stateMachineIndicators:
            le.setStyleSheet("background-color: rgb(255, 255, 255);")
        if self.success:
            self.stateMachineIndicators[7].setStyleSheet("background-color: rgb(0, 255, 0);")
        else:
            self.stateMachineIndicators[7].setStyleSheet("background-color: rgb(255, 0, 0);")
        self.refreshAll()

    def returnToGraspPoseSlot(self):
        self.clearRemoteIndicators()
        if self.devel:
            self.success = True
            self.plan = None
        else:
            self.plan, self.success = self.planning.grasp_return()
        for le in self.stateMachineIndicators:
            le.setStyleSheet("background-color: rgb(255, 255, 255);")
        if self.success:
            self.stateMachineIndicators[8].setStyleSheet("background-color: rgb(0, 255, 0);")
        else:
            self.stateMachineIndicators[8].setStyleSheet("background-color: rgb(255, 0, 0);")
        self.refreshAll()

    def releaseGraspSlot(self):
        name = str(self.comboBox.currentText())
        if name != '':
            self.clearRemoteIndicators()
            if self.devel:
                self.success = True
            else:
                self.success = self.planning.release_object(name)
            self.plan = None
            for le in self.stateMachineIndicators:
                le.setStyleSheet("background-color: rgb(255, 255, 255);")
            if self.success:
                self.stateMachineIndicators[9].setStyleSheet("background-color: rgb(0, 255, 0);")
            else:
                self.stateMachineIndicators[9].setStyleSheet("background-color: rgb(255, 0, 0);")
            self.refreshAll()

    def retreatSlot(self):
        self.clearRemoteIndicators()
        if self.devel:
            self.success = True
            self.plan = None
        else:
            self.plan, self.success = self.planning.pre_grasp_return()

        for le in self.stateMachineIndicators:
            le.setStyleSheet("background-color: rgb(255, 255, 255);")
        if self.success:
            self.stateMachineIndicators[10].setStyleSheet("background-color: rgb(0, 255, 0);")
        else:
            self.stateMachineIndicators[10].setStyleSheet("background-color: rgb(255, 0, 0);")
        self.refreshAll()

    def stowSlot(self):
        self.clearRemoteIndicators()
        if self.devel:
            self.success = True
        else:
            self.plan, self.success = self.planning.go_to_home(wait=False)
        for le in self.stateMachineIndicators:
            le.setStyleSheet("background-color: rgb(255, 255, 255);")
        if self.success:
            self.stateMachineIndicators[11].setStyleSheet("background-color: rgb(0, 255, 0);")
        else:
            self.stateMachineIndicators[11].setStyleSheet("background-color: rgb(255, 0, 0);")
        self.refreshAll()

    def readySlot(self):
        self.clearRemoteIndicators()
        if self.devel:
            self.success = True
        else:
            self.plan, self.success = self.planning.go_to_ready(wait=False)
        for le in self.stateMachineIndicators:
            le.setStyleSheet("background-color: rgb(255, 255, 255);")
        if self.success:
            self.stateMachineIndicators[12].setStyleSheet("background-color: rgb(0, 255, 0);")
        else:
            self.stateMachineIndicators[12].setStyleSheet("background-color: rgb(255, 0, 0);")
        self.refreshAll()

    def stopSlot(self):
        self.clearRemoteIndicators()
        if self.devel:
            self.success = True
        else:
            self.success = self.planning.stop()
        self.plan = None
        for le in self.stateMachineIndicators:
            le.setStyleSheet("background-color: rgb(255, 255, 255);")
        if self.success:
            self.stateMachineIndicators[13].setStyleSheet("background-color: rgb(0, 255, 0);")
        else:
            self.stateMachineIndicators[13].setStyleSheet("background-color: rgb(255, 0, 0);")
        self.refreshAll()

    def speedSliderMovedSlot(self, value):
        speed = float(value)/100.0
        self.refreshAll()

    def groundedActionCallback(self, msg):
        print("NLU: Received GroundedAction Message")
        print("    name: " + msg.name)
        print("")
        params = msg.params
        if len(params) == 0:
            print("NLU: Message contains no groundings")
            return;

        self.last_command = self.command
        self.command = {
            'constraint': None,
            'object': None,
            'reference_relation': None
        }
        for param in params:
            print("    " + param.name + ": " + param.value)

            if (param.name == 'reference'):
                self.command['object'] = param.value
            else:
                self.command[param.name] = param.value

        # Update state machine using the following convention
        #
        # The following ignore the value of 'reference_relation'
        #   'object' == pregrasp_pose --> goToPreGraspPoseSlot
        #   'object' == grasp_pose --> goToGraspPoseSlot
        #   'object' == sample_location --> goToSampleLocationSlot

        if (self.command['constraint'] == 'stop'):
                print("NLU: Calling stopSlot")
                self.stopSlot()
        # Currently 'inside' constraints are used for moving
        # to pregrasp/grasp locations
        if (self.command['constraint'] == 'inside'):
            # Handle cases where task is determined by the reference object
            if (self.command['object'] == 'pregrasp_pose'):
                print("NLU: Calling goToPreGraspPoseSlot")
                self.goToPreGraspPoseSlot()
            elif (self.command['object'] == 'grasp_pose'):
                print("NLU: Calling goToGraspPoseSlot")
                self.goToGraspPoseSlot()
            elif (self.command['object'] == 'sample_location'):
                print("NLU: Calling goToSampleLocationSlot")
                self.goToSampleLocationSlot()
            # Otherwise, consider value of 'reference_relation'
            else:
                if (self.command['reference_relation'] == 'pregrasp'):
                    print("NLU: Calling goToPreGraspPoseSlot")
                    self.goToPreGraspPoseSlot()
                elif (self.command['reference_relation'] == 'grasp'):
                    print("NLU: Calling goToGraspPoseSlot")
                    self.goToGraspPoseSlot()
                elif (self.command['reference_relation'] == 'remove'):
                    print("NLU: Calling removeToolFromTraySlot")
                    self.removeToolFromTraySlot()
        elif (self.command['constraint'] == 'extract'):
            if (self.command['object'] == 'sample_location'):
                print("NLU: extractSampleSlot")
                self.extractSampleSlot()
        elif (self.command['constraint'] == 'take_sample'):
            if (self.command['object'] == 'sample_location'):
                print("NLU: extractSampleSlot")
                self.takeSampleSlot()
        elif (self.command['constraint'] == 'release'):
            print("NLU: releaseGraspSlot")
            self.releaseGraspSlot()
        elif (self.command['constraint'] == 'execute'):
            # Is this a command to execute a grasp
            if (self.last_command['constraint'] == 'inside') and ((self.last_command['reference_relation'] == 'grasp') or (self.last_command['object'] == 'grasp_pose')):
                print("NLU: Calling executeGraspSlot")
                self.executeGraspSlot()
            else:
                print("NLU: Calling executeSlot: ")
                self.executeSlot()
        elif (self.command['constraint'] == 'stow'):
            print("NLU: stowSlot")
            self.stowSlot()
        else:
            print("NLU: Unable to process grounded command:")
            print(self.command)

    def ros_subscribe_loop(self):
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            rate.sleep()

    # Unity interface
    def unity_plan_req_callback(self, msg):
        rospy.loginfo("Recieved endeffector goal from unity remote")
        self.last_plan_key = None
        self.last_request_key = msg.child_frame_id
        self.plan, self.success = self.planning.set_ee_goal(msg)

        # TODO: Commented this out to debug segfaults - possible that this
        # callback is called multiple times out of sync with main thread
        # if self.success:
        #     # Publish custom display trajectory message for Unity
        #     traj_msg = DisplayTrajectory()
        #     traj_msg.model_id = self.last_request_key # Put request ID in model_id
        #     traj_msg.trajectory_start = self.planning.group.get_current_state()
        #     traj_msg.trajectory.append(self.plan)
        #     attached_objects = self.planning.scene.get_attached_objects();
        #     for key, value in attached_objects.items():
        #       traj_msg.trajectory_start.attached_collision_objects.append(value);
        #     self.trajectory_pub.publish(traj_msg);

        for le in self.stateMachineIndicators:
            le.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.remoteExecuteIndicator.setStyleSheet("background-color: rgb(255, 255, 255);")
        if self.success:
            self.last_plan_key = msg.child_frame_id
            self.remotePlanIndicator.setStyleSheet("background-color: rgb(0, 255, 0);")
        else:
            self.remotePlanIndicator.setStyleSheet("background-color: rgb(255, 0, 0);")

        # Check for plan & execute situation - command key will be sent before
        # pose request for simultaneous commands
        if self.success and self.override and self.last_plan_key == self.command_key:
            self.executeSlot()


    def unity_tool_req_callback(self, msg):
        rospy.loginfo("Recieved tool goal from unity remote")
        # Parse message
        tool = msg.data.split(',')[0]
        index = self.comboBox.findText(tool, QtCore.Qt.MatchFixedString)
        key = msg.data.split(',')[1]

        self.last_plan_key = None
        self.last_request_key = key

        if index < 0:
            return
        elif index != self.comboBox.currentIndex():
            self.comboBox.setCurrentIndex(index)
        self.plan, self.success = self.planning.pre_grasp_approach(tool)
        for le in self.stateMachineIndicators:
            le.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.remoteExecuteIndicator.setStyleSheet("background-color: rgb(255, 255, 255);")
        if self.success:
            self.last_plan_key = key
            self.stateMachineIndicators[0].setStyleSheet("background-color: rgb(0, 255, 0);")
            self.remotePlanIndicator.setStyleSheet("background-color: rgb(0, 255, 0);")
        else:
            self.stateMachineIndicators[0].setStyleSheet("background-color: rgb(255, 0, 0);")
            self.remotePlanIndicator.setStyleSheet("background-color: rgb(255, 0, 0);")

        # Auto-execute plan if override box is checked
        if self.override and self.last_plan_key == self.command_key:
            self.executeSlot()

    def unity_gripper_cmd_callback(self, msg):
        if not self.override:
            print("Ignoring grip command. Remote override is not enabled.")
            return

        self.planning.send_gripper_command(msg.data)

    def unity_command_callback(self, msg):
        self.command_key = msg.data
        rospy.loginfo(f"Recieved plan execution request {self.command_key} from unity remote")

        # # Handle case where command_key pertains to gripper
        # grip_cmds = ["grip_open", "grip_close"]
        # if not self.override and self.command_key in grip_cmds:
        #     print("Ignoring grip command. Remote override is not enabled.")
        #     return
        # elif self.command_key == "grip_open":
        #     self.openGripSlot()
        #     print("Opening gripper based on remote command.")
        #     return
        # elif self.command_key == "grip_close":
        #     self.closeGripSlot()
        #     print("Closing gripper based on remote command.")
        #     return

        # Check if there's a plan already made for this command key
        if self.command_key == self.last_plan_key and self.success:
            self.remoteExecuteIndicator.setStyleSheet("background-color: rgb(0, 255, 0);")
            # Execute plan if override box is checked
            if self.override:
                self.executeSlot()

    def unity_users_callback(self, msg):
        current_user = str(self.comboBox_3.currentText())
        shore, users = msg.data.split(':')
        users = users.split(',')
        self.users[shore] = users
        user_list = set()
        user_list.add("")
        for users in self.users.values():
            for user in users:
                user_list.add(user)

        all_items = [str(self.comboBox_3.itemText(i)) for i in range(self.comboBox_3.count())]
        for i, name in enumerate(all_items):
            if name not in user_list:
                self.comboBox_3.removeItem(i)
                if name == current_user:
                    self.comboBox_3.setCurrentIndex(0)
        for name in user_list:
            if name not in all_items:
                self.comboBox_3.addItem(name)

    def moveit_status_callback(self, msg):
        output_msg = String()
        output_msg.data = f"moveit {self.last_request_key} {msg.data}"
        self.status_pub.publish(output_msg)

    def activeUserChangeSlot(self):
        self.clearRemoteIndicators()
        name = str(self.comboBox_3.currentText())
        msg = String()
        msg.data = name
        self.active_scientist_pub.publish(msg)

    def cameraFpsChangeSlot(self):
        curr_text = str(self.comboBox_4.currentText())
        msg = Float32()

        if curr_text == "Unlimited":
            msg.data = 0 # Set 0 for unlimited
        else:
            msg.data = float(curr_text)

        self.camera_fps_pub.publish(msg)

    def activeTopicsChangeSlot(self):
        active = []
        topics = ["/left_img_topic", "/fish_img_topic", "/rgbd_img_topic", "/pcl_topic"]
        if self.checkBox.isChecked():
            active.append(topics[0])
        if self.checkBox_2.isChecked():
            active.append(topics[1])
        if self.checkBox_3.isChecked():
            active.append(topics[2])
        if self.checkBox_4.isChecked():
            active.append(topics[3])
        msg = String()
        msg.data = ",".join(active)
        self.active_topics_pub.publish(msg)

    def overrideChangeSlot(self):
        self.override = self.checkBox_5.isChecked()

        msg = Bool()
        msg.data =  self.override
        self.override_pub.publish(msg)
        if self.override:
            print("Remote override enabled. Use caution when executing remote commands")

    def controlModeChangeSlot(self):
        try:
            if self.radioButton.isChecked():
                success = self.switch_controller(['position_trajectory_controller'],['joint_position_controller'],1,True,2)
                if success:
                    print("Switched to position_trajectory_controller\n")
                else:
                    print("Failed switching to position_trajectory_controller\n")
            else:
                self.load_controller('joint_position_controller')
                success = self.switch_controller(['joint_position_controller'],['position_trajectory_controller'],1,True,2)
                if success:
                    print("Switched to joint_position_controller\n")
                else:
                    print("Failed switching to joint_position_controller\n")
        except rospy.ServiceException as e:
            print("Switch controller service call failed: %s"%e)

    def connectArmSlot(self):
        try:
            success = self.connect_arm()
            if success:
                self.connectIndicator.setStyleSheet("background-color: rgb(0, 255, 0);")
            else:
                self.connectIndicator.setStyleSheet("background-color: rgb(255, 0, 0);")
        except:
            pass

    def disconnectArmSlot(self):
        try:
            success = self.disconnect_arm()
            if success:
                self.connectIndicator.setStyleSheet("background-color: rgb(255, 0, 0);")
            else:
                self.connectIndicator.setStyleSheet("background-color: rgb(255, 255, 0);")
        except:
            pass

def parse_args():
    parser = argparse.ArgumentParser(description='Tool for annotating monocular image sequences with groundtruth poses.')
    parser.add_argument('--camera', type=str, required=True,
        help='Camera calibration yaml file in camchain format. Expected camera name is cam0.')
    parser.add_argument('--images', type=str, required=True,
        help='Folder containing image sequence. Expected naming convention is consecutive number sequence starting from 0.png.')
    parser.add_argument('--poses', type=str, required=True,
        help='CSV file containing pose annotations for image sequence. Expected format for each line in file is "image_number, x, y, z, qw, qx, qy, qz".')
    return parser.parse_args()

if __name__ == "__main__":
    # args = parse_args()
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = MainWindowUIClass(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
