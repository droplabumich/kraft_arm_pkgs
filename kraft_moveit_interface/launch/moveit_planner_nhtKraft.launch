<launch>
  <!-- specify the planning pipeline -->
  <arg name="pipeline" default="ompl" />

  <!-- By default, we do not start a database (it can be large) -->
  <arg name="db" default="false" />
  <!-- Allow user to specify database location -->
  <arg name="db_path" default="$(find kraft_nui_moveit_config)/default_warehouse_mongo_db" />

  <!-- the "sim" argument controls whether we connect to a Simulated or Real robot -->
  <!--  - if sim=false, a robot_ip argument is required -->
  <arg name="sim" default="true" />
  <arg name="robot_ip" unless="$(arg sim)" />

  <!-- By default, we are not in debug mode -->
  <arg name="debug" default="false" />

  <!--
  By default, hide joint_state_publisher's GUI

  MoveIt's "demo" mode replaces the real robot driver with the joint_state_publisher.
  The latter one maintains and publishes the current joint configuration of the simulated robot.
  It also provides a GUI to move the simulated robot around "manually".
  This corresponds to moving around the real robot without the use of MoveIt.
  -->
  <arg name="rviz_tutorial" default="false" />
  <arg name="use_gui" default="false" />


  <!-- The planning and execution components of MoveIt! configured to run -->
  <!-- using the ROS-Industrial interface. -->
 <!-- Non-standard joint names:
       - Create a file [robot_moveit_config]/config/joint_names.yaml
           controller_joint_names: [joint_1, joint_2, ... joint_N]
       - Update with joint names for your robot (in order expected by rbt controller)
       - and uncomment the following line: -->
  <rosparam command="load" file="$(find kraft_nht_moveit_config)/config/joint_names.yaml"/>

  <!-- load the robot_description parameter before launching ROS-I nodes -->
  <include file="$(find kraft_nht_moveit_config)/launch/planning_context.launch" >
    <arg name="load_robot_description" value="true" />
  </include>

  <!-- remap topics to conform to ROS-I specifications -->
  <!-- <remap from="/position_trajectory_controller/follow_joint_trajectory" to="/joint_trajectory_action" />
  <remap from="/position_trajectory_controller/state" to="/feedback_states" />
  <remap from="/position_trajectory_controller/command" to="/joint_path_command"/> -->

  <!-- If needed, broadcast static tf for robot root -->
    <node pkg="tf2_ros" type="static_transform_publisher" name="virtual_joint_broadcaster_0" args="0 0 0 0 0 0 world vehicle" />

  <!-- run the robot simulator and action interface nodes -->
  <group if="$(arg sim)">
    <include file="$(find industrial_robot_simulator)/launch/robot_interface_simulator.launch" />
  </group>

  <!-- run the "real robot" interface nodes -->
  <!--   - this typically includes: robot_state, motion_interface, and joint_trajectory_action nodes -->
  <!--   - replace these calls with appropriate robot-specific calls or launch files -->
  <group unless="$(arg sim)">
    <include file="$(find kraft_ros_control_boilerplate)/kraft_control/launch/kraft_hardware.launch" >
      <!-- <arg name="robot_ip" value="$(arg robot_ip)"/> -->
      <!-- <arg name="robot_ip" value="1"/> -->
    </include>
  </group>

  <!-- publish the robot state (tf transforms) -->
  <group if="$(arg sim)">
    <!-- <node name="robot_state_publisher" pkg="robot_state_publisher" type="robot_state_publisher"/> -->
    <!-- We do not have a robot connected, so publish fake joint states -->
    <node name="joint_state_publisher" pkg="joint_state_publisher" type="joint_state_publisher">
      <param name="use_gui" value="$(arg use_gui)"/>
      <rosparam param="source_list">[move_group/fake_controller_joint_states]</rosparam>
    </node>
  </group>

  <!-- Given the published joint states, publish tf for the robot links -->
  <node name="robot_state_publisher" pkg="robot_state_publisher" type="robot_state_publisher" respawn="true" output="screen" />


  <include file="$(find kraft_nht_moveit_config)/launch/move_group.launch">
    <arg name="allow_trajectory_execution" value="true"/>
    <arg name="publish_monitored_planning_scene" value="true" />
    <!-- <arg name="sim" value="$(arg sim)" /> -->
    <arg name="pipeline" value="$(arg pipeline)"/>
    <arg name="info" value="true"/>
    <arg name="debug" value="$(arg debug)"/>
    <arg name="fake_execution" value="$(arg sim)"/>
  </include>

  <!-- <node name="move_group_interface" pkg="kraft_basic_grasping" type="move_group_interface.py" output="screen">
      <group unless="$(arg sim)">
        <remap from = "/joint_states" to="/kraft/joint_states" />
      </group>
  </node> -->

   <!-- <node name="sample_space" pkg="kraft_basic_grasping" type="sample_space.py" output="screen">
      <group unless="$(arg sim)">
        <remap from = "/joint_states" to="/kraft/joint_states" />
      </group>
  </node> -->

  <include file="$(find kraft_nht_moveit_config)/launch/moveit_rviz.launch">
    <arg name="config" value="true"/>
  </include>

  <!-- If database loading was enabled, start mongodb as well -->
  <include file="$(find kraft_nui_moveit_config)/launch/default_warehouse_db.launch" if="$(arg db)">
    <arg name="moveit_warehouse_database_path" value="$(arg db_path)"/>
  </include>

</launch>
