#!/usr/bin/env python3
import rospy
from geometry_msgs.msg import TransformStamped, PoseStamped, Pose, Point, Quaternion
from sensor_msgs.msg import JointState
from std_msgs.msg import Header
import tf2_geometry_msgs
import tf2_ros
import numpy as np

class NUIDoors(object):
    def __init__(self):
        rospy.init_node('nui_doors', anonymous=True)

        self.tf_buffer = tf2_ros.Buffer(rospy.Duration(1200.0)) # tf buffer length
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer)
        self.tf_br = tf2_ros.TransformBroadcaster()

        self.pub_topic = rospy.get_param('joint_state_topic', '/door_joint_states')
        self.pub_door_pose = rospy.Publisher(self.pub_topic, JointState, tcp_nodelay=True, queue_size=10)
        # self.pub_door_pose_2 = rospy.Publisher('door_joint_states', JointState, tcp_nodelay=True, queue_size=10)

        self.SLAM_FISHEYE_FRAME = 'tagslam_fisheye'
        self.SLAM_STEREO_FRAME = 'tagslam_camera_left'
        self.SLAM_VEHICLE_FRAME = 'tagslam_vehicle'
        self.SLAM_DOOR_FRAME = 'tagslam_door_starboard'
        self.FISHEYE_FRAME = 'fisheye'
        self.STEREO_FRAME = 'CameraLeft'
        self.ARM_FRAME = 'Base'

        # self.default_joint_values = [1.5708, -1.5708]
        self.default_joint_values = [0.9, -1.3]
        self.last_joint_values = self.default_joint_values
        self.offset_port = np.array([1.044, -0.6478])
        self.offset_starboard = np.array([1.044, 0.5963])
        self.angle_offset_port = np.pi - 30.*np.pi/180.
        self.angle_offset_starboard = 3*np.pi/2. - 4.*np.pi/180.

        self.tf_base_starboard = TransformStamped()
        self.tf_base_starboard.header.frame_id = self.ARM_FRAME
        self.tf_base_starboard.child_frame_id = self.SLAM_DOOR_FRAME
        self.tf_base_starboard.transform.translation = Point(0.15, 0.147, -0.0181393322794)
        self.tf_base_starboard.transform.rotation = Quaternion(0, 0, 1, 0)

        self.spoof = True

    def door_pub(self, joint_values):
        msg = JointState()
        msg.header = Header()
        msg.header.stamp = rospy.Time.now()
        msg.name = ['SwingPort', 'SwingStarboard']
        msg.position = joint_values
        msg.velocity = []
        msg.effort = []
        self.pub_door_pose.publish(msg)
        # self.pub_door_pose_2.publish(msg)

    def reduce_angle(self, phi):
        if phi > np.pi:
            phi = phi - 2*np.pi
        elif phi < -np.pi:
            phi = phi + 2*np.pi
        return phi

    def door_angle_calc(self):
        try:
            tf_port = self.tf_buffer.lookup_transform(self.SLAM_VEHICLE_FRAME, self.SLAM_STEREO_FRAME, rospy.Time(0))
            tf_starboard = self.tf_buffer.lookup_transform(self.SLAM_VEHICLE_FRAME, self.SLAM_DOOR_FRAME, rospy.Time(0))
            t_port = tf_port.transform.translation
            t_port = np.array([t_port.x, t_port.y]) - self.offset_port
            angle_port = np.arctan2(t_port[1], t_port[0]) - self.angle_offset_port
            angle_port = self.reduce_angle(angle_port)
            t_starboard = tf_starboard.transform.translation
            t_starboard = np.array([t_starboard.x, t_starboard.y]) - self.offset_starboard
            angle_starboard = np.arctan2(t_starboard[1], t_starboard[0]) - self.angle_offset_starboard
            angle_starboard = self.reduce_angle(angle_starboard)
        except Exception as e:
            print(str(e))
            return self.last_joint_values
        self.last_joint_values = [angle_port, angle_starboard]
        return self.last_joint_values

    def update(self):
        # Arm to stereo
        try:
            tf_door = self.tf_buffer.lookup_transform(self.SLAM_DOOR_FRAME, self.SLAM_STEREO_FRAME, rospy.Time(0))
            pose_door = PoseStamped()
            pose_door.pose = Pose(tf_door.transform.translation, tf_door.transform.rotation)
            pose_door.header.frame_id = self.SLAM_DOOR_FRAME
            pose_base = tf2_geometry_msgs.do_transform_pose(pose_door, self.tf_base_starboard)
            tf_base_stereo = TransformStamped()
            tf_base_stereo.header.stamp = tf_door.header.stamp
            tf_base_stereo.header.frame_id = self.ARM_FRAME
            tf_base_stereo.child_frame_id = self.STEREO_FRAME
            tf_base_stereo.transform.translation = pose_base.pose.position
            tf_base_stereo.transform.rotation = pose_base.pose.orientation
            self.tf_br.sendTransform(tf_base_stereo)
        except Exception as e:
            print(str(e))

        # Door angles
        if not self.spoof:
            joint_angles = self.door_angle_calc()
            self.door_pub(joint_angles)
        else:
            self.door_pub(self.default_joint_values)

def main():
    spoof = rospy.get_param('spoof', False)

    nuidoors = NUIDoors()
    nuidoors.spoof = spoof

    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        # if spoof:
            # nuidoors.door_pub(nuidoors.default_joint_values)
        # else:
            # joint_angles = nuidoors.door_angle_calc()
            # nuidoors.door_pub(joint_angles)
            # nuidoors.update()
        nuidoors.update()
        rate.sleep()

if __name__ == '__main__':
    main()
