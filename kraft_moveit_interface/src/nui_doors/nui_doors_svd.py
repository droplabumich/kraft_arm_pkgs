#!/usr/bin/env python
import rospy
import sys
import message_filters
from std_msgs.msg import Float64, Float64MultiArray, Int32, Float32
from sensor_msgs.msg import JointState
from geometry_msgs.msg import TransformStamped
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from scipy import io
from scipy import optimize
import math
import tf
import tf2_ros

# If feedback pot message does not have header, run bag file with --clock and run "rosparam set use_sim_time true"
# before running this node

CAM_FRAME = 'body_fisheye'
VEHICLE_FRAME = 'body_vehicle'
DOOR_FRAME = 'body_door_starboard'
JOINTS = ['DoorPort', 'DoorStarboard']

SLAM_FISHEYE_FRAME = 'body_fisheye'
SLAM_STEREO_FRAME = 'body_camera_left'
SLAM_VEHICLE_FRAME = 'body_vehicle'
SLAM_DOOR_FRAME = 'body_door_starboard'
FISHEYE_FRAME = 'fisheye'
STEREO_FRAME = 'camera_left'
ARM_FRAME = 'Base'

MODE = 'CAPTURE'
# MODE = 'PROCESS'

tf2_buffer = tf2_ros.Buffer(rospy.Duration(1200.0))
tf2_listener = tf2_ros.TransformListener(tf2_buffer)
tf_listener = tf.TransformListener()


def calc_R(x,y, xc, yc):
    """ calculate the distance of each 2D points from the center (xc, yc) """
    return np.sqrt((x-xc)**2 + (y-yc)**2)

def f(c, x, y):
    """ calculate the algebraic distance between the data points and the mean circle centered at c=(xc, yc) """
    Ri = calc_R(x, y, *c)
    return Ri - Ri.mean()

def leastsq_circle(x,y):
    # coordinates of the barycenter
    x_m = np.mean(x)
    y_m = np.mean(y)
    center_estimate = x_m, y_m
    center, ier = optimize.leastsq(f, center_estimate, args=(x,y))
    xc, yc = center
    Ri       = calc_R(x, y, *center)
    R        = Ri.mean()
    residu   = np.sum((Ri - R)**2)
    return center, R, residu


## Calibrator class

class Calibrator(object):

    def __init__(self, **kwargs):
        self.points = {}
        self.points['port'] = []
        self.points['starboard'] = []
        self.tfs = {}
        self.tfs['port'] = []
        self.tfs['starboard'] = []

        self.t1_prev = []
        self.t2_prev = []

        elif MODE == 'PROCESS':
            self.process_points()


    def run(self):
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            try:
                tf_port = tf_buffer.lookup_transform(self.SLAM_VEHICLE_FRAME, self.SLAM_STEREO_FRAME, rospy.Time(0))
                tf_starboard = tf_buffer.lookup_transform(self.SLAM_VEHICLE_FRAME, self.SLAM_DOOR_FRAME, rospy.Time(0))
                t1 = tf_port.transform.translation
                t2 = tf_starboard.transform.translation
                if !np.array_equal(t1,self.t1_prev):
                    self.t1_prev = t1
                    self.tfs['port'].append(t1)
                    print "Points count port: {}      \r".format(len(self.tfs['port'])),
                if !np.array_equal(t2,self.t2_prev):
                    self.t2_prev = t2
                    self.tfs['starboard'].append(t2)
                    print "Points count starboard: {}      \r".format(len(self.tfs['starboard'])),
            except Exception as e:
                pass
            rate.sleep()

    def process_points(self, filename=TARGET+'.mat'):
        self.tfs = io.loadmat(filename)
        for j in range(len(self.tfs[LINKS[0]])):
            for i, joint in enumerate(JOINTS):
                if i in [0, 1]:
                    point = self.tfs[LINKS[0]][j][:3,3].T
                else:
                    # point = np.dot(np.linalg.inv(tf[LINKS[i-2]]), tf[LINKS[i-1]])[:3,3].T
                    point = self.tfs[LINKS[i-1]][j][:3,3].T
                self.points[joint].append(point)
        self.pot_feedback = self.tfs['pots']

    def translate_matrix(self, x=0.0, y=0.0, z=0.0):
        """ matrix to translate from coordinates (x,y,z) or a vector x"""
        matrix = np.identity(4, 'f')
        matrix[:3, 3] = [x, y, z]
        return matrix

    def quaternion_matrix(self, q):
        """ Create 4x4 rotation matrix from quaternion q """
        # q = [w, x, y, z]
        q = q / np.linalg.norm(q)  # only unit quaternions are valid rotations.
        nxx, nyy, nzz = -q[1]*q[1], -q[2]*q[2], -q[3]*q[3]
        qwx, qwy, qwz = q[0]*q[1], q[0]*q[2], q[0]*q[3]
        qxy, qxz, qyz = q[1]*q[2], q[1]*q[3], q[2]*q[3]
        return np.array([[2*(nyy + nzz)+1, 2*(qxy - qwz),   2*(qxz + qwy),   0],
                         [2 * (qxy + qwz), 2 * (nxx + nzz) + 1, 2 * (qyz - qwx), 0],
                         [2 * (qxz - qwy), 2 * (qyz + qwx), 2 * (nxx + nyy) + 1, 0],
                         [0, 0, 0, 1]], 'f')

    def fit_joint_space(self, points):
        space_dict = {}
        center = np.mean(points, axis=0)
        npoints = points - center # verify row broadcasting is correct
        u, s, vh = np.linalg.svd(npoints, full_matrices=True)
        N = vh[2,:]
        X = vh[0,:]
        Y = np.cross(N, X)
        points2d = np.zeros((len(npoints),2))
        points2d[:,0] = np.dot(npoints,X.T)
        points2d[:,1] = np.dot(npoints,Y.T)
        center, R, residu = leastsq_circle(points2d[:,0], points2d[:,1])
        return points2d, center, R, residu

    def pots_to_angles(self, points2d, center, r):
        points = points2d - center
        angles = []
        pots = []
        joint_idx = JOINTS.index(TARGET)
        for i, point in enumerate(points):
            angles.append(math.atan2(point[1], point[0]) * 180 / math.pi)
            pots.append(self.pot_feedback[i][joint_idx])
        return pots, angles

    def save(self, filename='door_cal.mat'):
        import pdb; pdb.set_trace()
        print("Saving data to {}".format(filename))
        io.savemat(filename, self.tfs)

def main():
    rospy.init_node('calibrator', anonymous=True)
    # rate = rospy.Rate(1000)
    # while not rospy.is_shutdown():
    #     # plt.pause(0.05)
    #     # app.process_events()
    #     rate.sleep()
    calibrator = Calibrator()
    if MODE == 'CAPTURE':
        calibrator.run()
        calibrator.save()
    elif MODE == 'PROCESS':
        pass
        # points = np.array(calibrator.points[TARGET])
        # points2d, center, r, residu = calibrator.fit_joint_space(points)
        # pots, angles = calibrator.pots_to_angles(points2d, center, r)


        # circle = plt.Circle((center[0], center[1]), r, color='blue', fill=False)

        # fig = plt.figure()

        # ax1 = plt.subplot(121)
        # ax1.scatter(points2d[:,0], points2d[:,1])
        # ax1.add_artist(circle)
        # ax1.axis('equal')

        # ax2 = plt.subplot(122)
        # ax2.scatter(pots, angles)

        # plt.pause(0.05)
        # raw_input()

if __name__ == '__main__':
    main()