#!/usr/bin/env python
import rospy
from geometry_msgs.msg import TransformStamped, PoseStamped, Pose
from sensor_msgs.msg import JointState
from std_msgs.msg import Header
import tf2_geometry_msgs
import tf2_ros
import numpy as np

rospy.init_node('door_tf', anonymous=True)

tf_buffer = tf2_ros.Buffer(rospy.Duration(1200.0)) # tf buffer length
tf_listener = tf2_ros.TransformListener(tf_buffer)
tf_br = tf2_ros.TransformBroadcaster()

SLAM_FISHEYE_FRAME = 'body_fisheye'
SLAM_STEREO_FRAME = 'body_camera_left'
SLAM_VEHICLE_FRAME = 'body_vehicle'
SLAM_DOOR_FRAME = 'body_door_starboard'
FISHEYE_FRAME = 'fisheye'
STEREO_FRAME = 'camera_left'
ARM_FRAME = 'Base'

tf_fisheye = tf_buffer.lookup_transform(SLAM_FISHEYE_FRAME, SLAM_DOOR_FRAME, rospy.Time(0), rospy.Duration(1))
pose_fisheye = PoseStamped()
pose_fisheye.pose = Pose(tf_fisheye.transform.translation, tf_fisheye.transform.rotation)
pose_fisheye.header.frame_id = FISHEYE_FRAME
tf_base = tf_buffer.lookup_transform(ARM_FRAME, FISHEYE_FRAME, rospy.Time(0))
pose_base = tf2_geometry_msgs.do_transform_pose(pose_fisheye, tf_base)
print(pose_base)