#!/usr/bin/env python
import rospy
import tf2_ros
import tf
from tf import transformations as tfs
import tf_conversions
from geometry_msgs.msg import TransformStamped, PoseStamped, Pose, Point, Quaternion
import tf2_geometry_msgs
from std_msgs.msg import Header
from tf2_msgs.msg import TFMessage
import numpy as np

rospy.init_node('orb_slam_ferry', anonymous=True)

tf2_buffer = tf2_ros.Buffer()
tf2_listener = tf2_ros.TransformListener(tf2_buffer)
tf2_br = tf2_ros.TransformBroadcaster()

tf_listener = tf.TransformListener()

inverse_base = True

def tf_from_pose(pose, stamp, frame_id, child_frame_id):
    t = TransformStamped()
    t.header.stamp = stamp
    t.header.frame_id = frame_id
    t.child_frame_id = child_frame_id
    t.transform.translation = pose.position
    t.transform.rotation = pose.orientation
    return t

def pose_from_tf(t, stamp):
    pose = PoseStamped()
    pose.header.stamp = stamp
    pose.header.frame_id = t.header.frame_id
    pose.pose = Pose(t.transform.translation, t.transform.rotation)
    return pose

def ferry_tf():
    # tf_listener.waitForTransform("body_fisheye", frame_id, rospy.Time(0), rospy.Duration(1.0))
    # (trans, rot) = tf_listener.lookupTransform('body_fisheye', frame_id, rospy.Time(0))
    # base_from_cam = Pose(Point(1.6883585359, 0.0855481678787, 0.921942197292), Quaternion(0.488593713868, 0.736646564096, -0.390901342284, -0.256562200985))
    # tf_cam = tf_from_pose(base_from_cam, stamp, "camera_left", "Base")
    # if inverse_base:

    stamp = rospy.Time.now()
    try:
        tf_base = tf2_buffer.lookup_transform('Base', 'vehicle', rospy.Time(0))
        try:
            tf_orb = tf2_buffer.lookup_transform('world', 'camera_left', rospy.Time(0))
        except:
            tf_orb = tf_from_pose(Pose(Point(0, 0, 0), Quaternion(0, 0, 0, 1)), stamp, "world", "camera_left")

        trans = np.array([1.6883585359, 0.0855481678787, 0.921942197292])
        rot = np.array([0.488593713868, 0.736646564096, -0.390901342284, -0.256562200985])
        t = tfs.concatenate_matrices(tfs.translation_matrix(trans), tfs.quaternion_matrix(rot))
        t_inv = tfs.inverse_matrix(t)
        trans_inv = tfs.translation_from_matrix(t_inv)
        rot_inv = tfs.quaternion_from_matrix(t_inv)
        tf_cam = tf_from_pose(Pose(Point(*trans_inv), Quaternion(*rot_inv)), stamp, "camera_left", "Base")

        pose_base = pose_from_tf(tf_base, stamp)
        pose_cam = tf2_geometry_msgs.do_transform_pose(pose_base, tf_cam)
        pose_orb = tf2_geometry_msgs.do_transform_pose(pose_cam, tf_orb)

        t_vehicle = tf_from_pose(pose_orb.pose, stamp, "world", "vehicle")
        tf2_br.sendTransform(t_vehicle)
        # t_world = tf_from_pose(Pose(Point(0, 0, 0), Quaternion(0, 0, 0, 1)), stamp, "world", "OrbSlamMap")
        # tf2_br.sendTransform(t_world)
    except Exception as e:
        print(str(e))

def handle_tf(msg):
    for t in msg.transforms:
        if t.child_frame_id == 'camera_left':
            ferry_tf(t.child_frame_id, t.header.stamp)

def main():
    # tf_sub = rospy.Subscriber("/tf", TFMessage, handle_tf)
    rate = rospy.Rate(100)
    while not rospy.is_shutdown():
        ferry_tf()
        rate.sleep()

if __name__ == '__main__':
    main()