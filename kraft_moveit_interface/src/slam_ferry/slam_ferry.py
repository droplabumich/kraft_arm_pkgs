#!/usr/bin/env python
import rospy
import tf2_ros
import tf
import tf_conversions
from geometry_msgs.msg import TransformStamped, PoseStamped, Pose
import tf2_geometry_msgs
from std_msgs.msg import Header
from tf2_msgs.msg import TFMessage

rospy.init_node('slam_ferry', anonymous=True)

tf2_buffer = tf2_ros.Buffer()
tf2_listener = tf2_ros.TransformListener(tf2_buffer)
tf2_br = tf2_ros.TransformBroadcaster()

tf_listener = tf.TransformListener()

detection_dict = {'body_pushcore': 'pushcore'}

def transform_tf(pose):
    try:
        pose_stamped = PoseStamped()
        pose_stamped.pose = pose
        pose.header.frame_id = 'fisheye'
        transform = tf2_buffer.lookup_transform('Base',
            pose.header.frame_id, #source frame
            rospy.Time(0), #get the tf at first available time
            rospy.Duration(1.0)) #wait for 1 second
    except Exception as e:
        print(str(e))
        return None
    pose_transformed = tf2_geometry_msgs.do_transform_pose(pose, transform)
    return pose_transformed

def ferry_tf(frame_id, stamp):
    # tf_listener.waitForTransform("body_fisheye", frame_id, rospy.Time(0), rospy.Duration(1.0))
    # (trans, rot) = tf_listener.lookupTransform('body_fisheye', frame_id, rospy.Time(0))
    import pdb; pdb.set_trace()
    transform = tf2_buffer.lookup_transform('body_fisheye', frame_id, rospy.Time(0))
    base_pose = transform_tf(Pose(transform.transform.translation, transform.transform.rotation))
    # import pdb; pdb.set_trace()
    if base_pose is not None:
        t = TransformStamped()
        t.header.stamp = stamp
        t.header.frame_id = 'Base'
        t.child_frame_id = detection_dict[frame_id]
        t.transform.translation = base_pose.pose.position
        t.transform.rotation = base_pose.pose.orientation
        tf2_br.sendTransform(t)

def handle_tf(msg):
    for t in msg.transforms:
        if t.child_frame_id in list(detection_dict.keys()):
            ferry_tf(t.child_frame_id, t.header.stamp)

def main():
    tf_sub = rospy.Subscriber("/tf", TFMessage, handle_tf)
    rospy.spin()

if __name__ == '__main__':
    main()