#!/usr/bin/env python

# ROS imports
import rospy
import rospkg
import message_filters
import tf
import tf2_ros
import tf2_geometry_msgs
from geometry_msgs.msg import PoseStamped, Pose

# Python imports
import time
import datetime
import os, sys
from os import path as osp
import numpy as np
import regex as re
from itertools import chain
from transforms3d import quaternions as q

class TFSaver():
    
    def __init__(self):
        rospy.init_node('tf_saver')

        self.tf_dict = {}
        self.frames = []

        self.target_frame = 'body_camera_left'
        # self.angle_format = 'axis'
        self.angle_format = 'quat'

        # Save paths
        try:
            self.storage_path = rospy.get_param("/tf_saver/storage_path")
        except(KeyError):
            self.storage_path = self.fullpath("package://kraft_moveit_interface")
        try:
            os.makedirs(osp.join(self.storage_path))
        except OSError, e:
            if e.errno != 17:
                raise

        # Output csv
        self.file = open(os.path.join(self.storage_path, "base_to_stereo_tf.txt"), "w")

        # Synchronized subscribers
        self.tl = tf.TransformListener()
        self.tf_buffer = tf2_ros.Buffer(rospy.Duration(1200.0)) # tf buffer length
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer)

        # Listen to tf until node is shutdown 
        self.listen()

    def __del__(self):
        self.file.close()

    def fullpath(self, relative_path):
        prefix = 'package://'
        rospack = rospkg.RosPack()
        if relative_path.startswith(prefix):
            relative_path = relative_path[len(prefix):]
            path_list = relative_path.split('/')
            pkg_path = rospack.get_path(path_list[0])
            path = osp.join(pkg_path, *path_list[1:])
        else:
            path = relative_path
        return path

    def save(self, pose):
        rospy.loginfo("Writing tf file {}".format(osp.join(self.storage_path, "base_to_stereo_tf.txt")))
        if self.angle_format == 'quat':
            self.file.write("x y z qx qy qz qw <source frame> <child frame>\n")
        elif self.angle_format == 'axis':
            self.file.write("x y z vx vy vz <source frame> <child frame>\n")
        t = pose.pose.position
        r = pose.pose.orientation
        if self.angle_format == 'quat':
            self.file.write("{} {} {} {} {} {} {} {} {}\n".format(
                t.x, t.y, t.z, r.x, r.y, r.z, r.w, 'Base', 'camera_left'))
        elif self.angle_format == 'axis':
            r = self.quat_to_axis(r.w, r.x, r.y, r.z)
            self.file.write("{} {} {} {} {} {} {} {}\n".format(
                t.x, t.y, t.z, r[0], r[1], r[2], 'Base', 'camera_left'))

    def quat_to_axis(self, w, x, y ,z):
        vec, angle = q.quat2axangle([w,x,y,z])
        vec = vec / np.linalg.norm(vec)
        if angle == 0:
            angle += 2*np.pi
        return vec*angle

    def fill_pose(self, x=0, y=0, z=0, qx=0, qy=0, qz=0, qw=1, frame='Base'):
        pose = PoseStamped()
        pose.header.frame_id = frame
        pose.pose.position.x = x
        pose.pose.position.y = y
        pose.pose.position.z = z
        pose.pose.orientation.x = qx
        pose.pose.orientation.y = qy
        pose.pose.orientation.z = qz
        pose.pose.orientation.w = qw
        return pose

    def listen(self):
        rate = rospy.Rate(1)
        rate.sleep()
        # self.tl.waitForTransform("/body_fisheye", "/body_camera_left", rospy.Time(), rospy.Duration(4.0))
        # self.tl.waitForTransform("/Base", "/fisheye", rospy.Time(), rospy.Duration(4.0))
        # (t_1, r_1) = self.tl.lookupTransform("/Base", "/fisheye", rospy.Time(0))
        # (t_2, r_2) = self.tl.lookupTransform("/body_fisheye", "/body_camera_left", rospy.Time(0))
        tf1 = self.tf_buffer.lookup_transform("Base", "fisheye", rospy.Time(0), rospy.Duration(5.0))
        tf2 = self.tf_buffer.lookup_transform("body_fisheye", "body_camera_left", rospy.Time(0), rospy.Duration(5.0))
        t = tf2.transform.translation
        r = tf2.transform.rotation
        pose = self.fill_pose(t.x, t.y, t.z, r.x, r.y, r.z, r.w)
        pose_transformed = tf2_geometry_msgs.do_transform_pose(pose, tf1)
        self.save(pose_transformed)

if __name__ == '__main__':
    tf_saver = TFSaver()
