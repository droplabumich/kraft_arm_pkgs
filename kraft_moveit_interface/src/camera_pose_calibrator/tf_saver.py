#!/usr/bin/env python
"""@@ This node labels and saves the images of the node it subscribes to @@"""

# ROS imports
import rospy
import rospkg
import message_filters
import tf

# Python imports
import time
import datetime
import os, sys
from os import path as osp
import numpy as np
import regex as re
from itertools import chain
from transforms3d import quaternions as q

class TFSaver():
    
    def __init__(self):
        rospy.init_node('tf_saver')

        self.tf_dict = {}
        self.frames = []

        self.target_frame = 'body_camera_left'
        # self.angle_format = 'axis'
        self.angle_format = 'quat'

        # Save paths
        try:
            self.storage_path = rospy.get_param("/tf_saver/storage_path")
        except(KeyError):
            self.storage_path = self.fullpath("package://kraft_moveit_interface")
        try:
            os.makedirs(osp.join(self.storage_path))
        except OSError, e:
            if e.errno != 17:
                raise

        # Output csv
        self.file = open(os.path.join(self.storage_path, "tf_saver.txt"), "w")

        # Synchronized subscribers
        self.tl = tf.TransformListener()

        # Listen to tf until node is shutdown 
        self.listen()

    def __del__(self):
        self.file.close()

    def fullpath(self, relative_path):
        prefix = 'package://'
        rospack = rospkg.RosPack()
        if relative_path.startswith(prefix):
            relative_path = relative_path[len(prefix):]
            path_list = relative_path.split('/')
            pkg_path = rospack.get_path(path_list[0])
            path = osp.join(pkg_path, *path_list[1:])
        else:
            path = relative_path
        return path

    def save(self):
        rospy.loginfo("Writing tf file {}".format(osp.join(self.storage_path, "tf.txt")))
        self.file.write("source frame: {}\n".format(self.target_frame))
        if self.angle_format == 'quat':
            self.file.write("child frame, x, y, z, qw, qx, qy, qz\n")
        elif self.angle_format == 'axis':
            self.file.write("child frame, x, y, z, vx, vy, vz\n")
        for frame in self.tf_dict:
            t = self.tf_dict[frame][0]
            r = self.tf_dict[frame][1]
            if self.angle_format == 'quat':
                self.file.write("{},{},{},{},{},{},{},{}\n".format(
                    frame, t[0], t[1], t[2], r[3], r[0], r[1], r[2]))
            elif self.angle_format == 'axis':
                r = self.quat_to_axis(r[3], r[0], r[1], r[2])
                self.file.write("{},{},{},{},{},{},{}\n".format(
                    frame, t[0], t[1], t[2], r[0], r[1], r[2]))

    def quat_to_axis(self, w, x, y ,z):
        vec, angle = q.quat2axangle([w,x,y,z])
        vec = vec / np.linalg.norm(vec)
        if angle == 0:
            angle += 2*np.pi
        return vec*angle

    def listen(self):
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            all_frames = re.findall(r'Frame (.*?) exists with parent (.*?)\.\n', self.tl.allFramesAsString())
            all_frames = list(set(list(chain.from_iterable(all_frames))))
            for frame in all_frames:
                if frame != self.target_frame:
                    try:
                        (t,r) = self.tl.lookupTransform(self.target_frame, frame, rospy.Time(0))
                    except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException) as e:
                        continue
                    self.tf_dict[frame] = (t,r)
            rate.sleep()
        self.save()

if __name__ == '__main__':
    tf_saver = TFSaver()
