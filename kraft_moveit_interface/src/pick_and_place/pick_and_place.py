#!/usr/bin/env python3
import sys
import copy
import rospy
import rospkg
import moveit_commander
import numpy as np
import time
import getch
import tf
import tf2_ros
import tf2_geometry_msgs
import actionlib
from transforms3d.euler import euler2quat
from os import path as osp
import yaml

from std_msgs.msg import String
from pyquaternion import Quaternion
from geometry_msgs.msg import PoseStamped, Pose, Point, Quaternion, Vector3, TransformStamped
from std_msgs.msg import UInt8, Empty, Header, ColorRGBA
from visualization_msgs.msg import Marker, MarkerArray
from moveit_msgs.msg import Constraints, JointConstraint, OrientationConstraint, DisplayTrajectory
from moveit_commander.conversions import pose_to_list
from control_msgs.msg import GripperCommandAction, GripperCommandGoal

from interactive_sample_location import SampleLocation


TOPIC_DTP = 'move_group/display_planned_path'
TOPIC_DES_POSE = "kraft_arm/des_pose"  # topics for desired pose
TOPIC_RVIZ_DISP_MARKERS = "kraft_arm/visualization_msgs/MarkerArray"
TOPIC_RVIZ_DISP_MARKER = "kraft_arm/visualization_msgs/Marker"
TOPIC_GRIPPER = "kraft_arm/gripper"
TOPIC_CURRENT_POSE = "kraft_arm/ee_pose"
TOPIC_UPDATE_GOAL_STATE = "/rviz/moveit/update_goal_state"
TOPIC_TOOLS = "/kraft/detected_tools"
TOPIC_TFS = "/unity_interface/tfs"
TOPIC_STATUS = "/kraft/pick_and_place_status"

# slam ferry publishes object poses from TagSLAM to tf
OBJECT_DICT = {'pushcore': 'pushcore',
               'fake_handle': 'pushcore',
               'silhonet_handle': 'pushcore'}

OBJECT_TYPE_DICT = {'handle': "package://kraft_moveit_interface/meshes/WHOI_THandle_decimated.stl",
                    'pushcore': "package://kraft_moveit_interface/meshes/WHOI_Pushcore_TagMount.stl",
                    'xrf': "package://kraft_moveit_interface/meshes/XRF/xrf_aprilmount_decimated.stl"}

class PickAndPlace(object):
    def __init__(self):
        moveit_commander.roscpp_initialize(sys.argv)
        self.robot = moveit_commander.RobotCommander()
        self.scene = moveit_commander.PlanningSceneInterface()
        self.group = moveit_commander.MoveGroupCommander("arm")
        self.gripper_group = moveit_commander.MoveGroupCommander("gripper")

        # Set all planning times
        self.group.set_planning_time(1)
        self.gripper_group.set_planning_time(1)

        # We can get the name of the reference frame for this robot:
        self.planning_frame = self.group.get_planning_frame()
        print("============ Reference frame: %s" % self.planning_frame)

        # We can also print the name of the end-effector link for this group:
        self.eef_link = self.group.get_end_effector_link()
        print("============ End effector: %s" % self.eef_link)

        # We can get a list of all the groups in the robot:
        self.group_names = self.robot.get_group_names()
        print("============ Robot Groups:", self.robot.get_group_names())

        # Sometimes for debugging it is useful to print the entire state of the
        # robot:
        print("=================== Printing robot state in initialization ================================")
        print(self.robot.get_current_state())

        self.robot_name = rospy.get_param('robot', None)

        self.tf_buffer = tf2_ros.Buffer(rospy.Duration(1200.0)) # tf buffer length
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer)
        self.tf_br = tf2_ros.TransformBroadcaster()

        # subscriber for defined position
        self.pub_rviz_markers = rospy.Publisher(TOPIC_RVIZ_DISP_MARKERS, MarkerArray, tcp_nodelay=True, queue_size=1)
        self.pub_rviz_marker = rospy.Publisher(TOPIC_RVIZ_DISP_MARKER, Marker, tcp_nodelay=True, queue_size=1)
        self.pub_gripper = rospy.Publisher(TOPIC_GRIPPER, UInt8, tcp_nodelay=True, queue_size=1)
        self.pub_ee_pose = rospy.Publisher(TOPIC_CURRENT_POSE, PoseStamped, tcp_nodelay=True, queue_size=1)
        self.pub_update_goal_state = rospy.Publisher(TOPIC_UPDATE_GOAL_STATE, Empty, tcp_nodelay=True, queue_size=1)
        self.display_trajectory_publisher = rospy.Publisher(TOPIC_DTP, DisplayTrajectory, tcp_nodelay=True, queue_size=1)

        self.tools_pub = rospy.Publisher(TOPIC_TOOLS, TransformStamped, tcp_nodelay=True, queue_size=1)
        self.tf_pub = rospy.Publisher(TOPIC_TFS, TransformStamped, tcp_nodelay=True, queue_size=1)
        self.status_pub = rospy.Publisher(TOPIC_STATUS, String, tcp_nodelay=True, queue_size=1)

        self.sampler = SampleLocation(OBJECT_TYPE_DICT['handle'])

        # self.open_gripper()
        self.gripper_is_open = 0
        self.freeze_pose = None
        self.objects = {}
        self.co_dict = {}
        self.pre_return_pose = PoseStamped()
        self.return_pose = PoseStamped()

        self.group.set_goal_position_tolerance(0.02)
        self.group.set_goal_orientation_tolerance(0.05)
        # self.group.set_goal_tolerance(0)
        self.group.set_max_velocity_scaling_factor(1.0)
        self.group.set_max_acceleration_scaling_factor(1.0)
        # self.group.set_planner_id("BiTRRT")
        self.group.set_planner_id("RRTconnect")
        # self.group.set_planner_id("LBTRRT")
        # self.group.set_planner_id("PRM")
        self.gripper_group.set_goal_position_tolerance(0.02)
        self.gripper_group.set_goal_orientation_tolerance(0.05)
        # self.gripper_group.set_goal_tolerance(0)
        self.gripper_group.set_max_velocity_scaling_factor(1.0)
        self.gripper_group.set_max_acceleration_scaling_factor(1.0)
        # self.gripper_group.set_planner_id("RRTstar")
        # self.gripper_group.set_planner_id("LBTRRT")
        # self.gripper_group.set_planner_id("PRM")
        self.gripper_group.set_planner_id("RRTconnect")

        self.clear_world()

    def fullpath(self, relative_path):
        prefix = 'package://'
        rospack = rospkg.RosPack()
        if relative_path.startswith(prefix):
            relative_path = relative_path[len(prefix):]
            path_list = relative_path.split('/')
            pkg_path = rospack.get_path(path_list[0])
            path = osp.join(pkg_path, *path_list[1:])
        else:
            path = relative_path
        return path

    def object_names(self):
        return list(self.objects.keys())

    def object_types(self):
        return list(OBJECT_TYPE_DICT.keys())

    def object_type(self, name):
        return name.split('_')[0]
        # return OBJECT_DICT[name]

    def update_rviz_display(self):
        # scene = self.scene
        # marker_array = MarkerArray()
        # markers = []
        # for key in list(self.objects.keys()):
        #     if key in scene.get_known_object_names():
        #         frame = 'Base'
        #         pose = self.get_obj_pose(key, frame=frame).pose
        #         pose.position.z += 0.05
        #         marker = Marker(
        #                 type=Marker.TEXT_VIEW_FACING,
        #                 id=self.objects[key],
        #                 lifetime=rospy.Duration(),
        #                 pose=pose,
        #                 scale=Vector3(0.06, 0.06, 0.06),
        #                 header=Header(frame_id=frame),
        #                 color=ColorRGBA(0.0, 1.0, 0.0, 0.8),
        #                 text=key)
        #         markers.append(marker)
        # marker_array.markers = markers
        # self.pub_rviz_markers.publish(marker_array)
        self.pub_update_goal_state.publish(Empty())

    def remove_rviz_marker(self, unique_id):
        marker_array = MarkerArray()
        markers = []
        marker = Marker(
            type=Marker.TEXT_VIEW_FACING,
            id=unique_id,
            lifetime=rospy.Duration(0.01),
            pose=Pose(Point(0, 0, 0), Quaternion(0, 0, 0, 1)),
            scale=Vector3(0.06, 0.06, 0.06),
            header=Header(frame_id='Base'),
            color=ColorRGBA(0.0, 1.0, 0.0, 0),
            text="Remove")
        markers.append(marker)
        marker_array.markers = markers
        self.pub_rviz_markers.publish(marker_array)

    def get_unique_id(self):
        ids = list(self.objects.values())
        i = 0
        while i in ids:
            i += 1
        return i

    def go_to_named_goal(self, name, wait=True):
        self.group.set_named_target(name)
        self.group.set_goal_position_tolerance(0.02)
        self.group.set_goal_orientation_tolerance(0.05)
        # self.group.set_goal_tolerance(0)
        self.group.set_max_velocity_scaling_factor(1.0)
        self.group.set_max_acceleration_scaling_factor(1.0)
        success, plan, planning_time, error_code = self.group.plan()
        self.update_rviz_display()
        return plan, success


    def go_to_ready(self, wait=True):
        plan, success = self.go_to_named_goal("ready", wait)
        return plan, success

    def go_to_home(self, wait=True):
        plan, success = self.go_to_named_goal("home", wait)
        return plan, success

    def stop(self):
        self.group.stop()
        self.gripper_group.stop()
        self.update_rviz_display()

    def fill_pose(self, x=0, y=0, z=0, qx=0, qy=0, qz=0, qw=1, frame='Base'):
        pose = PoseStamped()
        pose.header.frame_id = frame
        pose.pose.position.x = x
        pose.pose.position.y = y
        pose.pose.position.z = z
        pose.pose.orientation.x = qx
        pose.pose.orientation.y = qy
        pose.pose.orientation.z = qz
        pose.pose.orientation.w = qw
        return pose

    def fill_pose_msg(self, pose_msg, frame='Base'):
        pose = PoseStamped()
        pose.header.frame_id = frame
        pose.pose.position.x = pose_msg.position.x
        pose.pose.position.y = pose_msg.position.y
        pose.pose.position.z = pose_msg.position.z
        pose.pose.orientation.x = pose_msg.orientation.x
        pose.pose.orientation.y = pose_msg.orientation.y
        pose.pose.orientation.z = pose_msg.orientation.z
        pose.pose.orientation.w = pose_msg.orientation.w
        return pose

    def transform_pose(self, pose, frame='Base'):
        try:
            transform = self.tf_buffer.lookup_transform(frame,
               pose.header.frame_id, #source frame
               rospy.Time(0), #get the tf at first available time
               rospy.Duration(1.0)) #wait for 1 second
        except Exception as e:
            print(str(e))
            return pose
        pose_transformed = tf2_geometry_msgs.do_transform_pose(pose, transform)
        return pose_transformed

    def get_obj_pose(self, obj_name, frame='Base'):
        # try:
        pose_w = self.scene.get_object_poses([obj_name])[obj_name]
        # except Exception as e:
        # return None
        pose_w = self.fill_pose_msg(pose_w, frame=self.group.get_planning_frame())
        pose = self.transform_pose(pose_w, frame='Base')
        return pose

    def detach_all(self):
        scene = self.scene
        group = self.group
        attached_objects = scene.get_attached_objects()
        ee_link = group.get_end_effector_link()
        for key in attached_objects.keys():
            scene.remove_attached_object(ee_link, key)

    def clear_world(self):
        scene = self.scene
        self.detach_all()
        for name in scene.get_known_object_names():
            # self.remove_mesh_from_scene(name)
            scene.remove_world_object(name)
        self.objects = {}

    def grasp_object(self, name):
        robot = self.robot
        group = self.group
        scene = self.scene
        touch_links = robot.get_link_names(group="gripper")
        ee_link = group.get_end_effector_link()
        self.pre_return_pose = self.get_thandle_grasp_pose(name, approach=True)
        self.return_pose = self.get_thandle_grasp_pose(name, approach=False)
        scene.attach_mesh(ee_link, name, touch_links=touch_links)
        self.wait_for_state_update(name, obj_is_attached=True, obj_is_known=False)
        scene.remove_world_object(name)
        success = self.close_gripper()
        if success:
            self.remove_rviz_marker(self.objects[name])
        else:
            scene.remove_attached_object(ee_link, name)
            self.wait_for_state_update(name, obj_is_attached=False, obj_is_known=True)
        return success


    def release_object(self, name):
        robot = self.robot
        group = self.group
        scene = self.scene
        ee_link = group.get_end_effector_link()
        success = self.open_gripper()
        if success:
            attached_objects = scene.get_attached_objects()
            ee_link = self.group.get_end_effector_link()
            tool = attached_objects[name] # message type moveit_msgs/AttachedCollisionObject
            pose = PoseStamped()
            pose.header.frame_id = ee_link
            pose.pose.position = tool.object.mesh_poses[0].position
            pose.pose.orientation = tool.object.mesh_poses[0].orientation
            pose_base = self.transform_pose(pose)
            transform = TransformStamped()
            transform.transform.translation = pose_base.pose.position
            transform.transform.rotation = pose_base.pose.orientation
            transform.child_frame_id = name
            transform.header.stamp = rospy.Time.now()
            transform.header.frame_id = 'Base'
            self.tf_br.sendTransform(transform)
            scene.remove_attached_object(ee_link, name)
            self.wait_for_state_update(name, obj_is_attached=False, obj_is_known=True)
            # self.pre_return_pose = PoseStamped()
            # self.return_pose = PoseStamped()
        return success

    def pre_grasp_approach(self, name):
        pose_gr = self.get_thandle_grasp_pose(name, approach=True)
        (plan, success) = self.plan_cartesian_path(pose_gr, scale=1)
        return plan, success

    def grasp_approach(self, name, notch=False):
        self.open_gripper()
        pose_gg = self.get_thandle_grasp_pose(name, approach=False, notch=notch)
        self.constrain_endeffector_orientation()
        (plan, success) = self.plan_cartesian_path(pose_gg, scale=1)
        return plan, success

    def grasp_retreat(self, name):
        group = self.group
        scene = self.scene
        robot = self.robot
        touch_links = robot.get_link_names(group="gripper")
        ee_link = group.get_end_effector_link()
        scene.remove_attached_object(ee_link, name)
        self.wait_for_state_update(name, obj_is_attached=False, obj_is_known=True)
        pose_gr = self.get_thandle_grasp_pose(name, approach=True)
        scene.attach_mesh(ee_link, name, touch_links=touch_links)
        self.wait_for_state_update(name, obj_is_attached=True, obj_is_known=False)
        scene.remove_world_object(name)
        self.constrain_endeffector_orientation()
        (plan, success) = self.plan_cartesian_path(pose_gr, scale=1)
        return plan, success

    def pre_grasp_return(self):
        (plan, success) = self.plan_cartesian_path(self.pre_return_pose, scale=1)
        return plan, success

    def grasp_return(self):
        self.constrain_endeffector_orientation()
        (plan, success) = self.plan_cartesian_path(self.return_pose, scale=1)
        return plan, success

    def sample(self, approach=False):
        pose = self.get_sample_pose(approach)
        # print("Here")
        # print(pose)
        # if not approach:
            # self.constrain_endeffector_orientation()
        (plan, success) = self.plan_cartesian_path(pose, scale=1)
        return plan, success

    def set_ee_goal(self, msg):
        # Convert from TransformStamped to PoseStamped
        pose_goal = PoseStamped()
        pose_goal.header = msg.header
        pose_goal.pose.orientation = msg.transform.rotation
        pose_goal.pose.position.x = msg.transform.translation.x
        pose_goal.pose.position.y = msg.transform.translation.y
        pose_goal.pose.position.z = msg.transform.translation.z
        (plan, success) = self.plan_cartesian_path(pose_goal)
        return plan, success


#### FUNCTION TO ENSURE THE OBJECT HAS BEEN ADDED/DELETED FROM THE PLANNING SCENE
    def wait_for_state_update(self, name, obj_is_known=False, obj_is_attached=False, timeout=0.001):
        scene = self.scene
        start = rospy.get_time()
        seconds = rospy.get_time()
        while (seconds - start < timeout) and not rospy.is_shutdown():
            attached_objects = scene.get_attached_objects([name])
            is_attached = len(attached_objects.keys()) > 0
            is_known = name in scene.get_known_object_names()
            if (obj_is_attached == is_attached) and (obj_is_known == is_known):
                return True
            rospy.sleep(0.0001)
            seconds = rospy.get_time()
        return False

    def send_gripper_command(self, cmd):
        self.pub_gripper.publish(UInt8(cmd))

    def send_ee_pose(self, current_pose):
        current_ee_pose = PoseStamped()
        current_ee_pose.header.stamp = rospy.get_rostime()
        current_ee_pose.pose = current_pose
        self.pub_ee_pose.publish(current_ee_pose)

    def open_gripper(self, wait=True):
        group = self.gripper_group
        # gripper_group_values = group.get_current_joint_values()
        # gripper_group_values[1] = 1.57
        # success = self.gripper_group.go(gripper_group_values, wait=wait)
        group.clear_pose_targets()
        # group.set_joint_value_target(gripper_group_values)
        group.set_named_target("open")
        self.publish_status("open_grip plan running")
        (success, plan, planning_time, error_code) = group.plan()
        # self.gripper_group.stop()
        group.clear_pose_targets()
        if success:
            self.publish_status("open_grip plan complete")
            self.send_gripper_command(0)
            try: # gripper controller only exists in sim
                self.publish_status("open_grip execute running")
                # TODO: execute should return true or false, but our path
                # tolerances are off and so this always returns false.
                # To make this more robust, we should fix this behavior
                group.execute(plan, wait=wait)
                self.publish_status("open_grip execute success")
            except:
                self.publish_status("open_grip execute failed")
                pass
        else:
            self.publish_status("open_grip plan failed")
        self.update_rviz_display()
        return success

    def close_gripper(self, wait=True):
        group = self.gripper_group
        # gripper_group_values = group.get_current_joint_values()
        # gripper_group_values[1] = -0.4
        # success = self.gripper_group.go(gripper_group_values, wait=wait)
        group.clear_pose_targets()
        # group.set_joint_value_target(gripper_group_values)
        group.set_named_target("close")
        self.publish_status("close_grip plan running")
        (success, plan, planning_time, error_code) = group.plan()
        # self.gripper_group.stop()
        group.clear_pose_targets()
        if success:
            self.publish_status("close_grip plan complete")
            self.send_gripper_command(255)
            try: # gripper controller only exists in sim
                self.publish_status("close_grip execute running")
                # TODO: execute should return true or false, but our path
                # tolerances are off and so this always returns false.
                # To make this more robust, we should fix this behavior
                group.execute(plan, wait=wait)
                self.publish_status("close_grip execute success")
            except:
                self.publish_status("close_grip execute failed")
                pass
        else:
            self.publish_status("close_grip plan failed")
        self.update_rviz_display()
        return success

    def moveit_add_mesh(self, name, pose, filename, size=(1, 1, 1)):
        """
        Add a mesh to the planning scene
        """
        scene = self.scene
        # Very naughty private function calls, but compatible with planning scene interface as is
        co = scene._PlanningSceneInterface__make_mesh(name, pose, filename, size)
        scene._PlanningSceneInterface__submit(co, attach=False)
        return co

    def add_mesh_to_scene(self, name, pose, filename, frame='Base'):
        if name in list(self.objects.keys()):
            print("{} already exists in scene! Must have unique object name.".format(name))
        pose_msg = PoseStamped()
        # pose_msg.header.frame_id = self.robot.get_planning_frame()
        pose_msg.header.frame_id = frame
        pose_msg.pose.position.x = pose.position.x
        pose_msg.pose.position.y = pose.position.y
        pose_msg.pose.position.z = pose.position.z
        pose_msg.pose.orientation.x = pose.orientation.x
        pose_msg.pose.orientation.y = pose.orientation.y
        pose_msg.pose.orientation.z = pose.orientation.z
        pose_msg.pose.orientation.w = pose.orientation.w
        self.co_dict[name] = self.moveit_add_mesh(name, pose_msg, filename)
        self.wait_for_state_update(name, obj_is_attached=False, obj_is_known=True)
        unique_id = self.get_unique_id()
        self.objects[name] =  unique_id
        self.update_rviz_display()

    def add_mesh_to_scene_pose_stamped(self, name, pose, filename):
        self.co_dict[name] = self.moveit_add_mesh(name, pose, filename)
        self.wait_for_state_update(name, obj_is_attached=False, obj_is_known=True)
        unique_id = self.get_unique_id()
        self.objects[name] =  unique_id
        self.update_rviz_display()

    def remove_mesh_from_scene(self, name):
        scene = self.scene
        group = self.group
        attached_objects = scene.get_attached_objects()
        if name in list(attached_objects.keys()):
            ee_link = group.get_end_effector_link()
            scene.remove_attached_object(ee_link, name)
            self.wait_for_state_update(name, obj_is_attached=False, obj_is_known=True)
        if name in scene.get_known_object_names():
            scene.remove_world_object(name)
            self.wait_for_state_update(name, obj_is_attached=False, obj_is_known=False)
        if name in list(self.objects.keys()):
            unique_id = self.objects[name]
            self.remove_rviz_marker(unique_id)
            del self.objects[name]
            del self.co_dict[name]

    def display_plan(self, plan):
        robot = self.robot
        # rospy.sleep(0.1)
        display_trajectory = DisplayTrajectory()
        # display_trajectory.trajectory_start = robot.get_current_state()
        display_trajectory.trajectory_start = self.group.get_current_state()
        display_trajectory.trajectory.append(plan)
        attached_objects = self.scene.get_attached_objects();
        for key, value in attached_objects.items():
          display_trajectory.trajectory_start.attached_collision_objects.append(value);
        # Publish
        self.display_trajectory_publisher.publish(display_trajectory);

#### FUNCTION TO MOVE THE ARM
    def execute_plan(self, plan, wait=True):
        if plan is not None:
            # state = self.robot.get_current_state()
            # self.group.set_start_state(state)
            self.group.set_start_state_to_current_state()
            try:
                self.publish_status("pose execute running")
                # TODO: execute should return true or false, but our path
                # tolerances are off and so this always returns false.
                # To make this more robust, we should fix this behavior
                self.group.execute(plan, wait=wait)
                self.publish_status("pose execute success")
            except:
                self.publish_status("pose execute failed")
                pass
            self.group.stop()
            self.group.clear_pose_targets()
            self.update_rviz_display()

### GENERAL FUNCTION TO PLAN THE PATH OF THE ROBOT (NO RESTRICTIONS)
    def plan_cartesian_path(self, pose, scale=1):
        group = self.group
        # group.allow_replanning(True)
        group.allow_replanning(False)
        group.set_start_state_to_current_state()
        group.set_pose_reference_frame(pose.header.frame_id)
        group.set_goal_position_tolerance(0.02)
        group.set_goal_orientation_tolerance(0.05)
        # group.set_goal_tolerance(0)
        group.set_max_velocity_scaling_factor(1.0)
        group.set_max_acceleration_scaling_factor(1.0)
        # waypoints = []
        # wpose = self.des_pose
        # waypoints.append(copy.deepcopy(pose.pose))
        # (plan, fraction) = group.compute_cartesian_path(waypoints,  # waypoints to follow
                                                        # 0.01,  # eef_step
                                                        # 0.0)  # jump_threshold
        group.clear_pose_targets()
        group.set_pose_target(pose)
        self.publish_status("pose plan running")
        (success, plan, planning_time, error_code) = group.plan()
        group.clear_path_constraints()
        if success:
            self.publish_status("pose plan complete")
            return plan, success
        else:
            print("Plan failed: {}".format(error_code))
            self.publish_status("pose plan failed")
            return None, success
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)

    def constrain_endeffector_orientation(self):
        current_pose = self.group.get_current_pose()
        current_pose = self.transform_pose(current_pose, frame='Base').pose
        constraints = Constraints()
        orientation_constraint = OrientationConstraint()
        orientation_constraint.link_name = self.group.get_end_effector_link()
        orientation_constraint.header.frame_id = "Base"
        orientation_constraint.orientation = current_pose.orientation
        orientation_constraint.absolute_x_axis_tolerance = 0.2
        orientation_constraint.absolute_y_axis_tolerance = 0.2
        orientation_constraint.absolute_z_axis_tolerance = 0.2
        orientation_constraint.weight = 1
        constraints.name = "orientation lock"
        constraints.orientation_constraints.append(orientation_constraint)
        self.group.set_path_constraints(constraints)

    def get_thandle_normal_pose(self, pose_h, approach=False, notch=False, sample=False):
        quat_g_to_h = tf.transformations.quaternion_from_euler(np.pi/2, -np.pi/2, 0, 'rxyz')
        rh = pose_h.pose.orientation
        ph = pose_h.pose.position
        quat_h = [rh.x, rh.y, rh.z, rh.w]
        quat_g = tf.transformations.quaternion_multiply(quat_h, quat_g_to_h)
        if notch:
            dist = -0.08
        else:
            dist = -0.11
        if approach:
            if sample:
                dist += -0.26
            else:
                dist += -0.2
        Rg = tf.transformations.quaternion_matrix(quat_g)
        pos = np.array([dist, 0, 0, 1])
        pos_g = np.dot(Rg, pos)[:3] + np.array([ph.x, ph.y, ph.z])
        pose = self.fill_pose(*np.append(pos_g, quat_g))
        return pose

    def get_thandle_grasp_pose(self, name, approach=False, notch=False):
        pose_h = self.get_obj_pose(name, frame='Base')
        pose = self.get_thandle_normal_pose(pose_h, approach, notch=notch)
        return pose

    def get_sample_pose(self, approach=False):
        # q = tf.transformations.quaternion_from_euler(-np.pi, -np.pi/2, 0, 'rxyz')
        # pose = self.fill_pose(x=0, y=1., z=0, qx=q[0], qy=q[1], qz=q[2], qw=q[3], frame='Base')
        pose = self.sampler.getSamplePose()
        pose = self.transform_pose(pose)
        pose = self.get_thandle_normal_pose(pose, approach, sample=True)
        return pose

    def fake_object_generator(self):
        q = tf.transformations.quaternion_from_euler(np.pi/2, 0, 0, 'rxyz')
        pose_h = self.fill_pose(x=1.0, y=.35, z=0.2, qx=q[0], qy=q[1], qz=q[2], qw=q[3], frame='Base')
        # self.add_mesh_to_scene_pose_stamped('handle', pose_h, self.fullpath('package://kraft_moveit_interface/meshes/WHOI_THandle_OBJ/WHOI_THandle.obj'))
        # key = OBJECT_DICT['fake_handle']
        key = 'pushcore'
        self.add_mesh_to_scene_pose_stamped('pushcore_fake', pose_h, self.fullpath(OBJECT_TYPE_DICT[key]))

    def update_sample_model(self, name):
        # key = OBJECT_DICT[name]
        key = name.split('_')[0]
        path = OBJECT_TYPE_DICT[key]
        self.sampler.updateSampleMarker(path)

    def set_object_type(self, name, key):
        if OBJECT_DICT[name] == key:
            return
        else:
            OBJECT_DICT[name] = key
            co = self.co_dict[name]
            pose = self.fill_pose_msg(co.mesh_poses[0])
            self.remove_mesh_from_scene(name)
            self.add_mesh_to_scene_pose_stamped(name, pose, self.fullpath(OBJECT_TYPE_DICT[key]))
            self.update_sample_model(name)

    def update_slam(self):
        if not self.freeze_slam:
            self.nui_doors.update()

    def publish_tooltray_location(self):
        transform = TransformStamped()
        pose = self.fill_pose(x=-0.18528220057487488, y=-0.21311548352241516, z=0, qx=0.5, qy=0.5, qz=0.5, qw=0.5, frame='vehicle')
        transform.transform.translation = pose.pose.position
        transform.transform.rotation = pose.pose.orientation
        transform.child_frame_id = 'tooltray'
        transform.header.stamp = rospy.Time.now()
        transform.header.frame_id = 'vehicle'
        self.tools_pub.publish(transform)


    def update_objects(self):
        tf_yaml = self.tf_buffer.all_frames_as_yaml()
        tf_dict = yaml.load(tf_yaml, Loader=yaml.FullLoader)
        frame_list = []
        for name in list(OBJECT_TYPE_DICT.keys()):
            l = [s for s in tf_dict.keys() if name in str(s) and "tagslam" not in str(s)]
            if l:
                frame_list += l
        scene = self.scene
        attached_objects = scene.get_attached_objects()
        attached_object_names = list(attached_objects.keys())
        known_objects = scene.get_known_object_names()
        marker_array = MarkerArray()
        markers = []
        update_flag = False
        for frame_id in frame_list:
            frame_id = str(frame_id)
            try:
                if frame_id in attached_object_names:
                    tool = attached_objects[frame_id] # message type moveit_msgs/AttachedCollisionObject
                    ee_link = self.group.get_end_effector_link()
                    transform = TransformStamped()
                    transform.transform.translation = tool.object.mesh_poses[0].position
                    transform.transform.rotation = tool.object.mesh_poses[0].orientation
                    transform.child_frame_id = frame_id
                    transform.header.stamp = rospy.Time.now()
                    transform.header.frame_id = ee_link
                    self.tools_pub.publish(transform)
                else:
                    transform = self.tf_buffer.lookup_transform('Base', frame_id, rospy.Time(0))
                    t = transform.transform.translation
                    r = transform.transform.rotation
                    pose = self.fill_pose(t.x, t.y, t.z, r.x, r.y, r.z, r.w)

                    # Uncomment this for tool detections in RVIZ (needs debugging - causes errors in ROV autonomy master)
                    # if frame_id in known_objects and frame_id in list(self.co_dict.keys()) and frame_id != self.freeze_pose:
                    #     co = self.co_dict[frame_id]
                    #     co.mesh_poses = [pose.pose]
                    #     # Very naughty private function call, but compatible with planning scene interface as is
                    #     scene._PlanningSceneInterface__submit(co, attach=False)
                    #     update_flag = True
                    # elif frame_id != self.freeze_pose:
                    #     # TODO: Update this key parsing based on new tool tf naming convention
                    #     key = frame_id.split('_')[0] # Note: this always throws an error
                    #     self.add_mesh_to_scene_pose_stamped(frame_id, pose, self.fullpath(OBJECT_TYPE_DICT[key]))
                    #     update_flag = True
                    # else:
                    #     continue
                    #
                    # self.tools_pub.publish(transform)
                    ######

                    # Uncomment this for tool detections in RVIZ (needs debugging - causes errors in ROV autonomy master)
                    # pose.pose.position.z += 0.05
                    # marker = Marker(
                    #         type=Marker.TEXT_VIEW_FACING,
                    #         id=self.objects[frame_id],
                    #         lifetime=rospy.Duration(),
                    #         pose=pose.pose,
                    #         scale=Vector3(0.06, 0.06, 0.06),
                    #         header=Header(frame_id='Base'),
                    #         color=ColorRGBA(0.0, 1.0, 0.0, 0.8),
                    #         text=frame_id)
                    #
                    # markers.append(marker)
                    ####

            except Exception as e:
                # NOTE: Temporarily commenting this out - it spams tool detections due to the key error in `key = frame_id.split('_')[0]` line`
                # print("Error in pick and place:")
                # print(str(e))
                pass

        if update_flag:
            marker_array.markers = markers
            self.pub_rviz_markers.publish(marker_array)


    def publish_status(self, status):
        msg = String(status)
        self.status_pub.publish(msg)

##### MAIN LOOP OF THE PROGRAM
    def loop(self):
        # rospy.spin()
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            self.publish_tooltray_location()
            self.update_objects()
            rate.sleep()

    def run_once(self):
        rate = rospy.Rate(500)
        rate.sleep()

def main():
    rospy.init_node('kraft_pick_and_place', anonymous=True)
    try:
        planning = PickAndPlace()
        planning.clear_world()
        planning.update_objects()

        # planning.open_gripper()

        # planning.fake_object_generator()
        # plan, success = planning.pre_grasp_approach('handle')
        # if success:
            # planning.execute_plan(plan)
        # q = tf.transformations.quaternion_from_euler(np.pi/2, 0, 0, 'rxyz')
        # # q = tf.transformations.quaternion_from_euler(0, 0, 0, 'rxyz')
        # # pose_h = planning.fill_pose(x=0, y=1, z=0, qx=q[0], qy=q[1], qz=q[2], qw=q[3], frame='Base')
        # pose_h = planning.fill_pose(x=-.75, y=0.2, z=0, qx=q[0], qy=q[1], qz=q[2], qw=q[3], frame='Base')
        # planning.add_mesh_to_scene_pose_stamped('handle', pose_h, '/home/gidobot/workspace/dataset_toolboxes/DeepHandles_toolbox/WHOI_THandle/WHOI_THandle_OBJ/WHOI_THandle.obj')

        # planning.go_to_home()
        # pose_gr = planning.get_thandle_grasp_pose('handle', approach=True)
        # pose_gg = planning.get_thandle_grasp_pose('handle', approach=False)

        # (plan, success) = planning.plan_cartesian_path(pose_gr, scale=1)
        # if success:
        #     planning.execute_plan(plan)
        # planning.open_gripper()

        # (plan, success) = planning.plan_cartesian_path(pose_gg, scale=1)
        # if success:
        #     planning.execute_plan(plan)
        # planning.grasp_object('handle')

        # (plan, success) = planning.plan_cartesian_path(pose_gr, scale=1)
        # if success:
        #     planning.execute_plan(plan)

        # pose_gpr = copy.deepcopy(pose_gg)
        # pose_gpr.pose.position.y = 1.0
        # pose_gpr.pose.position.x = 0
        # pose_gpr.pose.position.z = 0
        # (plan, success) = planning.plan_cartesian_path(pose_gpr, scale=1)
        # if success:
        #     planning.execute_plan(plan)

        # pose_gp = copy.deepcopy(pose_gpr)
        # pose_gp.pose.position.z = -0.2
        # (plan, success) = planning.plan_cartesian_path(pose_gp, scale=1)
        # if success:
        #     planning.execute_plan(plan)

        # (plan, success) = planning.plan_cartesian_path(pose_gpr, scale=1)
        # if success:
        #     planning.execute_plan(plan)

        # (plan, success) = planning.plan_cartesian_path(pose_gr, scale=1)
        # if success:
        #     planning.execute_plan(plan)

        # (plan, success) = planning.plan_cartesian_path(pose_gg, scale=1)
        # if success:
        #     planning.execute_plan(plan)

        # planning.release_object('handle')

        # (plan, success) = planning.plan_cartesian_path(pose_gr, scale=1)
        # if success:
        #     planning.execute_plan(plan)

        # planning.close_gripper()
        # planning.go_to_home()

        planning.loop()
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == '__main__':
    main()
