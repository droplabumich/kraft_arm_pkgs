#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <cstdio>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/transform_listener.h>
#include <vector>
#include <algorithm>

// const std::vector<std::string> DETECTION_LIST = {"tagslam_pushcore_1", "tagslam_pushcore_2", "tagslam_xrf_1"};
// const std::vector<std::string> OBJECT_LIST = {"pushcore_1", "pushcore_2", "xrf_1"};
std::vector<std::string> DETECTION_LIST;
std::vector<std::string> OBJECT_LIST;
std::string CAMERA_FRAME_ID;
std::string TAGSLAM_CAMERA_FRAME_ID;


void print_tf(const geometry_msgs::TransformStamped &transform_msg) {
   tf::StampedTransform transform;
   tf::transformStampedMsgToTF(transform_msg, transform);
   double yaw, pitch, roll;
   transform.getBasis().getRPY(roll, pitch, yaw);
   tf::Quaternion q = transform.getRotation();
   tf::Vector3 v = transform.getOrigin();
   std::cout << "- Translation: [" << v.getX() << ", " << v.getY() << ", " << v.getZ() << "]" << std::endl;
   std::cout << "- Rotation: in Quaternion [" << q.getX() << ", " << q.getY() << ", "
             << q.getZ() << ", " << q.getW() << "]" << std::endl
             << "            in RPY (radian) [" <<  roll << ", " << pitch << ", " << yaw << "]" << std::endl
             << "            in RPY (degree) [" <<  roll*180.0/M_PI << ", " << pitch*180.0/M_PI << ", " << yaw*180.0/M_PI << "]" << std::endl;

   //print transform
}

geometry_msgs::PoseStamped transform_pose(const geometry_msgs::PoseStamped &pose) {
  static tf2_ros::Buffer tf2_buffer;
  static tf2_ros::TransformListener tf2_listener(tf2_buffer);

  geometry_msgs::TransformStamped transform;

  geometry_msgs::PoseStamped pose_stamped;
  pose_stamped.pose = pose.pose;
  pose_stamped.header.frame_id = CAMERA_FRAME_ID;
  try{
    // transform = tf2_buffer.lookupTransform("Base", "fisheye", pose.header.stamp);
    transform = tf2_buffer.lookupTransform("Base", CAMERA_FRAME_ID, ros::Time(0));
    tf2::doTransform(pose_stamped, pose_stamped, transform);
  }
  catch (tf::TransformException ex){
    // ROS_ERROR("%s",ex.what());
    ros::Duration(1.0).sleep();
  }
  return pose_stamped;
}

void ferry_tf(const int id_index, const ros::Time &stamp){
  std::string frame_id = DETECTION_LIST[id_index];

  static tf2_ros::Buffer tf2_buffer;
  static tf2_ros::TransformListener tf2_listener(tf2_buffer);
  static tf2_ros::TransformBroadcaster tf2_broadcaster;

  geometry_msgs::TransformStamped t_det;
  geometry_msgs::TransformStamped t_base;
  tf::StampedTransform t;

  try{
    t_det = tf2_buffer.lookupTransform(TAGSLAM_CAMERA_FRAME_ID, frame_id, stamp, ros::Duration(1.0));
    tf::transformStampedMsgToTF(t_det, t);
    tf::Quaternion q = t.getRotation();
    tf::Vector3 v = t.getOrigin();
    geometry_msgs::PoseStamped pose_base;
    geometry_msgs::PoseStamped pose_det;
    pose_det.pose.position.x = v.getX();
    pose_det.pose.position.y = v.getY();
    pose_det.pose.position.z = v.getZ();
    pose_det.pose.orientation.x = q.getX();
    pose_det.pose.orientation.y = q.getY();
    pose_det.pose.orientation.z = q.getZ();
    pose_det.pose.orientation.w = q.getW();
    pose_det.header.stamp = stamp;
    pose_base = transform_pose(pose_det);

    t_base.header.stamp = stamp;
    t_base.header.frame_id = "Base";
    t_base.child_frame_id = OBJECT_LIST[id_index];
    t_base.transform.translation.x = pose_base.pose.position.x;
    t_base.transform.translation.y = pose_base.pose.position.y;
    t_base.transform.translation.z = pose_base.pose.position.z;
    t_base.transform.rotation.x = pose_base.pose.orientation.x;
    t_base.transform.rotation.y = pose_base.pose.orientation.y;
    t_base.transform.rotation.z = pose_base.pose.orientation.z;
    t_base.transform.rotation.w = pose_base.pose.orientation.w;

    // print_tf(t_base);
    tf2_broadcaster.sendTransform(t_base);
  }
  catch (tf::TransformException ex){
    // ROS_ERROR("%s",ex.what());
    ros::Duration(1.0).sleep();
  }
}

void handle_tf(const tf2_msgs::TFMessage::ConstPtr &tf_list) {
  int id_index;
  for (auto const& t : tf_list->transforms) {
    auto result = std::find(DETECTION_LIST.begin(), DETECTION_LIST.end(), t.child_frame_id);
    if (result != DETECTION_LIST.end()){
      id_index = std::distance(DETECTION_LIST.begin(), result);
      ferry_tf(id_index, t.header.stamp);
    }
  }
}


int main(int argc, char** argv){
  ros::init(argc, argv, "slam_ferry");

  ros::NodeHandle node;

  // TODO: Figure out why global namespaces are needed - make this relative
  node.getParam("/slam_ferry/detection_list", DETECTION_LIST);
  node.getParam("/slam_ferry/object_list", OBJECT_LIST);
  node.getParam("/slam_ferry/camera_frame_id", CAMERA_FRAME_ID);
  node.getParam("/slam_ferry/tagslam_camera_frame_id", TAGSLAM_CAMERA_FRAME_ID);

  ros::Subscriber tf_sub = node.subscribe("/tf", 10, handle_tf);

  // tf::TransformListener listener;

  // listener.waitForTransform("/tagslam_fisheye", "/tagslam_pushcore", ros::Time(), ros::Duration(1.0));
  ros::spin();
  // ros::Rate rate(10.0);
  // while (node.ok()){
  //   rate.sleep();
  // }
  return 0;
}
