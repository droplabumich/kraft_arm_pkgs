#!/usr/bin/env python
import rospy
import tf2_ros
import tf
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Vector3
from std_msgs.msg import Header

def main():
    rospy.init_node('cam_odom')
    tf2_buffer = tf2_ros.Buffer()
    tf2_listener = tf2_ros.TransformListener(tf2_buffer)

    # pub_topic = rospy.get_param('joint_state_topic', '/kraft/joint_states')
    pub_topic = "/camera/fisheye/odom"
    pub_odom = rospy.Publisher(pub_topic, Odometry, tcp_nodelay=True, queue_size=10)

    rate = rospy.Rate(20) 
    while not rospy.is_shutdown():
        try:
            tf = tf2_buffer.lookup_transform('world', 'fisheye', rospy.Time())
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            rate.sleep()
            continue

        odom = Odometry()
        odom.header.stamp = tf.header.stamp
        odom.header.frame_id = 'world'
        odom.child_frame_id = 'fisheye_odom'
        odom.pose.pose = Pose(tf.transform.translation, tf.transform.rotation)
        pub_odom.publish(odom)

        rate.sleep()

if __name__ == '__main__':
    main()