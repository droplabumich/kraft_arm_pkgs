#!/usr/bin/env python
import rospy
from sensor_msgs.msg import JointState
import numpy as np

class KinCal(object):
    def __init__(self):
        rospy.init_node('kinematic_calibrator', anonymous=True)

        self.sub_joint_states = rospy.Subscriber('/kraft/joint_states', JointState, self.callback)
        self.pub_topic = rospy.get_param('joint_state_topic', '/calibrated_joint_states')
        self.pub_joint_states = rospy.Publisher(self.pub_topic, JointState, tcp_nodelay=True, queue_size=10)

        self.joint_names = ['Slew', 'Shoulder', 'Elbow', 'Wrist_Pitch', 'Wrist_Yaw', 'Grip_Roll']
        self.joint_offsets = [0, 0, 3., 0, 0, 0]

    def callback(self, msg):
        cal_msg = JointState()
        cal_msg.header = msg.header
        cal_msg.name = msg.name
        # cal_msg.position = msg.position
        for i, name in enumerate(msg.name):
            try:
                idx = self.joint_names.index(name)
            except Exception as e:
                cal_msg.position.append(msg.position[i])
                continue
            # import pdb; pdb.set_trace()
            cal_msg.position.append(msg.position[i] + self.joint_offsets[idx]*np.pi/180.)
        self.pub_joint_states.publish(cal_msg) 

def main():
    calibrator = KinCal()
    rospy.spin()

if __name__ == '__main__':
    main()