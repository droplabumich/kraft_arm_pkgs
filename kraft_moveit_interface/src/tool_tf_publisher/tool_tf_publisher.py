#!/usr/bin/env python3

import rospy
import tf2_ros
from geometry_msgs.msg import TransformStamped

import yaml

TOOLS = ["pushcore", "quiver", "xrf"]

class ToolsTfPublisher():
    def __init__(self):
        rospy.init_node("tools_publisher")
        self.rate = rospy.Rate(10)

        self.buffer = tf2_ros.Buffer()
        self.tf_listener = tf2_ros.TransformListener(self.buffer)

        self.tools_pub = rospy.Publisher("/kraft/detected_tools",
                                         TransformStamped,
                                         queue_size=1)
    def run(self):
        while not rospy.is_shutdown():

            frames_yaml = self.buffer.all_frames_as_yaml()
            frames_dict = yaml.safe_load(frames_yaml)


            for frame in frames_dict.keys():
                if self.checkTfName(frame):
                    try:
                        transform = self.buffer.lookup_transform('Base',
                                                                 frame,
                                                                 rospy.Time(0),
                                                                 rospy.Duration(1.0))
                    except (tf2_ros.LookupException,
                            tf2_ros.ConnectivityException,
                            tf2_ros.ExtrapolationException) as err:
                        rospy.logwarn_throttle(0.1, "Error in ToolsTfPublisher - is the apriltag tf tree connected to Base?")
                        self.rate.sleep()
                        continue
                    transform.child_frame_id = transform.child_frame_id.lstrip("detected_")
                    self.tools_pub.publish(transform)

            self.rate.sleep()

    def checkTfName(self, frame):
        frame = str(frame)
        if frame.startswith("detected"):
            for tool in TOOLS:
                if tool in frame:
                    return True
        return False


if __name__ == "__main__":
    tf_pub = ToolsTfPublisher()
    tf_pub.run()
