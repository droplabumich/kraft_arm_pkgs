#!/usr/bin/env python3

"""
keyboard_controller.py

Provides keybindings for controlling the arm joint-by-joint. In order to use
this, the "keyboard" option must be selected in the rov_autonomy_master GUI
window.
"""

import rospy
import sys
import getch
import curses
from std_msgs.msg import UInt8, Float64, Float64MultiArray, String
from sensor_msgs.msg import JointState
from copy import deepcopy
from math import pi
from threading import Thread, Lock

JOINTS = ['Slew', 'Shoulder', 'Elbow', 'Wrist_Pitch', 'Wrist_Yaw', 'Grip_Roll']

mutex = Lock()

class KeyboardController(object):

    def __init__(self, **kwargs):

        if rospy.has_param("/kraft/kraft/min_angle") and rospy.has_param("/kraft/kraft/max_angle"):
            self.min_angle = rospy.get_param("/kraft/kraft/min_angle")
            self.max_angle = rospy.get_param("/kraft/kraft/max_angle")
        else:
            sys.exit("kraft angle limit params missing. Exiting...")

        self.pub_gripper = rospy.Publisher('/kraft_arm/gripper', UInt8, queue_size=1)
        self.pub = rospy.Publisher('/kraft/joint_position_controller/command', Float64MultiArray, queue_size=10)
        self.sub = rospy.Subscriber('/kraft/joint_states', JointState, self.handle_joint_states, tcp_nodelay=True, queue_size=1)
        self.sub_char = rospy.Subscriber('/kraft/keyboard_controller_char', String, self.user_input_callback)

        self.char = None
        self.new_cmd = False

        self.current_joint_states = {}
        self.des_joint_states = {}
        for joint in JOINTS:
            self.current_joint_states[joint] = 0
            self.des_joint_states[joint] = 0
        self.delta = 0.005
        self.rate = rospy.Rate(100)
        self.init = False
        self.prev_time = rospy.Time.now().to_sec()

    def run(self):
        print('Controls: Slew <u,j>, Shoulder <i,k>, Elbow <o,l>, Wrist_Pitch <e,d>, Wrist_Yaw <r,f>, Wrist_Roll <t,g>, Gripper <y,h>')
        exit = False
        while not exit and not rospy.is_shutdown():
            if self.init and self.new_cmd:
                self.new_cmd = False

                # If more than a second has passed, update the keyboard
                # controller's understanding of the current joint states. This
                # is to prevent uncontrolled jumps when switching between this
                # controller and the moveit planner
                if rospy.Time.now().to_sec() - self.prev_time > 1:
                    self.des_joint_states = deepcopy(self.current_joint_states)

                if (self.char == 'q'):
                    exit = True
                # Slew
                elif (self.char == 'u'):
                    self.des_joint_states['Slew'] += self.delta
                    limit = self.max_angle[0]*pi/180.
                    if self.des_joint_states['Slew'] > limit:
                        self.des_joint_states['Slew'] = limit
                elif (self.char == 'j'):
                    self.des_joint_states['Slew'] -= self.delta
                    limit = self.min_angle[0]*pi/180.
                    if self.des_joint_states['Slew'] < limit:
                        self.des_joint_states['Slew'] = limit
                # Shoulder
                elif (self.char == 'i'):
                    self.des_joint_states['Shoulder'] += self.delta
                    limit = self.max_angle[1]*pi/180.
                    if self.des_joint_states['Shoulder'] > limit:
                        self.des_joint_states['Shoulder'] = limit
                elif (self.char == 'k'):
                    self.des_joint_states['Shoulder'] -= self.delta
                    limit = self.min_angle[1]*pi/180.
                    if self.des_joint_states['Shoulder'] < limit:
                        self.des_joint_states['Shoulder'] = limit
                # Elbow
                elif (self.char == 'o'):
                    self.des_joint_states['Elbow'] += self.delta
                    limit = self.max_angle[2]*pi/180.
                    if self.des_joint_states['Elbow'] > limit:
                        self.des_joint_states['Elbow'] = limit
                elif (self.char == 'l'):
                    self.des_joint_states['Elbow'] -= self.delta
                    limit = self.min_angle[2]*pi/180.
                    if self.des_joint_states['Elbow'] < limit:
                        self.des_joint_states['Elbow'] = limit
                # Wrist_Pitch
                elif (self.char == 'd'):
                    self.des_joint_states['Wrist_Pitch'] += self.delta
                    limit = self.max_angle[3]*pi/180.
                    if self.des_joint_states['Wrist_Pitch'] > limit:
                        self.des_joint_states['Wrist_Pitch'] = limit
                elif (self.char == 'e'):
                    self.des_joint_states['Wrist_Pitch'] -= self.delta
                    limit = self.min_angle[3]*pi/180.
                    if self.des_joint_states['Wrist_Pitch'] < limit:
                        self.des_joint_states['Wrist_Pitch'] = limit
                # Wrist_Yaw
                elif (self.char == 'r'):
                    self.des_joint_states['Wrist_Yaw'] += self.delta
                    limit = self.max_angle[4]*pi/180.
                    if self.des_joint_states['Wrist_Yaw'] > limit:
                        self.des_joint_states['Wrist_Yaw'] = limit
                elif (self.char == 'f'):
                    self.des_joint_states['Wrist_Yaw'] -= self.delta
                    limit = self.min_angle[4]*pi/180.
                    if self.des_joint_states['Wrist_Yaw'] < limit:
                        self.des_joint_states['Wrist_Yaw'] = limit
                # Wrist_Roll
                elif (self.char == 't'):
                    self.des_joint_states['Grip_Roll'] += self.delta
                    limit = self.max_angle[5]*pi/180.
                    if self.des_joint_states['Grip_Roll'] > limit:
                        self.des_joint_states['Grip_Roll'] = limit
                elif (self.char == 'g'):
                    self.des_joint_states['Grip_Roll'] -= self.delta
                    limit = self.min_angle[5]*pi/180.
                    if self.des_joint_states['Grip_Roll'] < limit:
                        self.des_joint_states['Grip_Roll'] = limit
                # Gripper open close
                elif (self.char == 'y'):
                    self.pub_gripper.publish(UInt8(0)) # open
                elif (self.char == 'h'):
                    self.pub_gripper.publish(UInt8(255)) # close
                else:
                    continue
                msg = Float64MultiArray()
                # if len(self.des_joint_states) == 7:
                #     self.des_joint_states = self.des_joint_states[:6]
                msg.data = [self.des_joint_states[x] for x in JOINTS]
                self.prev_time = rospy.Time.now().to_sec()
                print("Current: {}".format(self.current_joint_states))
                print("Desired: {}".format(self.des_joint_states))
                print('Controls: Slew <u,j>, Shoulder <i,k>, Elbow <o,l>, Wrist_Pitch <e,d>, Wrist_Yaw <r,f>, Wrist_Roll <t,g>, Gripper <y,h>')
                self.pub.publish(msg)

            self.rate.sleep()

    def handle_joint_states(self, data):
        for i, name in enumerate(data.name):
            self.current_joint_states[name] = data.position[i]
        if not self.init:
            self.des_joint_states = deepcopy(self.current_joint_states)
            self.init = True

    def user_input_callback(self, data):
        self.char = data.data
        self.new_cmd = True


def main():
    # ROS_WARNING("begining testing ****************************************************************");
    try:
        rospy.init_node('keyboard_controller', anonymous=True)
        controller = KeyboardController()
        controller.run()
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return

if __name__ == '__main__':
    main()
