#!/usr/bin/env python3

"""
keyboard_input.py

Sends keyboard presses as characters to the ROS topic
/kraft/keyboard_controller_char. This node is intended to be used with
the keyboard_controller. Because input is blocking, this node is separate
from the keyboard_controller to ensure that the keyboard controller remains
updated on the current state of the arm for safety reasons.

Note: This node doesn't guarantee that all keyboard presses are sent. This should
be fine for the intended use, but if it's critical that all keystrokes are
received then this code needs to be updated
"""
import rospy
import sys
import getch
import curses
from std_msgs.msg import String
from threading import Thread, Lock

mutex = Lock()

class KeyboardInput(object):
    def __init__(self, **kwargs):
        self.pub_char = rospy.Publisher('/kraft/keyboard_controller_char', String, queue_size=1)
        self.rate = rospy.Rate(100)

    def run(self):
        # ROS has a strange delay in publishing when the node is first created,
        # so let's give it some "cycles" to warm up
        for _ in range(100):
            self.rate.sleep()

        while not rospy.is_shutdown():
            char = getch.getche()
            char_msg = String(char)
            self.pub_char.publish(char_msg)

            self.rate.sleep()

def main():
    try:
        rospy.init_node('keyboard_input', anonymous=True)
        k_input = KeyboardInput()
        k_input.run()
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return

if __name__ == '__main__':
    main()
