#!/usr/bin/env python3

"""
gripper_manager.py

Since the kraft gripper has no built-in feedback, this node estimates
the gripper position based on the published commands.

The kraft gripper also has a tendency to 'lock up' the arm if the gripper is
trying to forcefully close when something is in the jaws, so this node
also sends an idle command if enough time has passed

Empirical data:
https://docs.google.com/spreadsheets/d/1qtzFzVYAEny2XoNrSDe3vfTVx7a-UNnFyxLFpTLa5Us/edit?usp=sharing
"""

import rospy
import math
from std_msgs.msg import UInt8, Float64

class GripperManager(object):

    def __init__(self):
        self.rate = rospy.Rate(100) # Assume this is sufficiently higher than gripper commands
        self.grip_cmd_sub = rospy.Subscriber('/kraft_arm/gripper', UInt8,
                                             self.grip_cmd_callback)
        self.grip_cmd_pub = rospy.Publisher('/kraft_arm/gripper', UInt8,
                                             queue_size=1)
        self.grip_pos_pub = rospy.Publisher('/kraft_arm/gripper_position_estimate', Float64,
                                            queue_size=1)
        self.last_cmd = 52 # 52 is 0 velocity
        self.last_cmd_time = rospy.get_time()
        self.cmd_timeout = -1 # How long to wait before issuing neutral command.
                              # Set to -1 for no timeout
        self.last_loop_time = rospy.get_time()

        # Assume gripper starts open, and that the gripper range is
        # close [-30, 50] open
        self.max_pos = 50*(math.pi/180)
        self.min_pos = -30*(math.pi/180)
        self.grip_pos = self.max_pos


    def grip_cmd_callback(self, msg):
        self.last_cmd = msg.data
        self.last_cmd_time = rospy.get_time()

    def run(self):
        while not rospy.is_shutdown():
            # If command has been 255 for awhile, don't grip so hard
            if (rospy.get_time() - self.last_cmd_time > self.cmd_timeout) and (self.last_cmd == 255) and (self.cmd_timeout != -1):
                stop_cmd = UInt8(65)
                self.grip_cmd_pub.publish(stop_cmd)

            # Assume 52 is stopped
            if self.last_cmd == 52:
                angular_vel = 0
            else:
                angular_vel = -0.0164*self.last_cmd + 0.84 # See link for data

            delta_t = rospy.get_time() - self.last_loop_time
            self.grip_pos += angular_vel*delta_t
            self.last_loop_time = rospy.get_time()

            if self.grip_pos > self.max_pos:
                self.grip_pos = self.max_pos
            elif self.grip_pos < self.min_pos:
                self.grip_pos = self.min_pos

            self.grip_pos_pub.publish(Float64(self.grip_pos))
            self.rate.sleep()



def main():
    try:
        rospy.init_node('gripper_manager')
        gripper_manager = GripperManager()
        gripper_manager.run()
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return

if __name__ == '__main__':
    main()
