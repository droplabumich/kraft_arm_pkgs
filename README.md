# kraft_arm_pkgs

This repository contains a collection of ROS packages that are used to connect
to and control the kraft arm.

### kraft_arm_description
This package contains the meshes and URDF files for the arm. The meshes we most
commonly use are the folders `meshes/KRAFTARM` which contains models of the arm, and
`meshes/nUIKraft` which contains the models for the NUI vehicle's
body, doors, stereo camera, and wrist-mounted fisheye camera. The URDF files
we most commonly use are the `kraft.urdf` (arm description only) and
`nUIKraft.urdf` (arm and vehicle description). These URDF files reference
the mesh files and describe the way the different arm links are connected to
each other.

#### Quirks
<!-- TODO: verify if this is true, add instructions on how to switch between testbed and vehicle arm -->
+ Vehicle and testbed arm's wrist roll differ by 30 degrees

### kraft_basic_grasping
<!-- TODO: can we remove this? I don't see references to it in other repos -->
Deprecated?

### kraft_control_gui
<!-- TODO: can we remove this -->
Deprecated?

### kraft_moveit_interface
This package contains a collection of custom utilities created for interfacing
with the arm.

The primary launch file we use when we want to test with the arm is
`movit_planner_nuiKraft.launch`. This launch file also has the `sim` arg which
defaults to `False`. When running in sim mode, the launch file won't run the
low-level node that interfaces with the hardware, but will still set up the
arm models in MoveIt.

<!-- TODO: fill out details here -->
Utilities (in `src` folder):
+ `cam_odom`
+ `camera_pose_calibrator`
+ `interactive_sample_location`
+ `keyboard_controller` - contains a ROS node that takes in keyboard inputs
and controls the arm joint-by-joint
+ `kinematic_calibrator`
+ `nui_doors`- uses the output from tagslam to update the door positions
+ `orb_slam_ferry`
+ `pick_and_place` - contains pipeline for sending MoveIt commands to the
pre-grasp location, tool grasp location, and tool return location based on the
current tf tree. Also handles visualizing tool meshes in MoveIt/RVIZ
(Note: MoveIt will plan treat the tools as obstacles and will attempt to find
  a plan that avoids collisions with the tools)
+ `tagslam_tf_republisher`- republishes the tf between tagslam and the base tag
so that the tf tree is fully connected
+ `slam_ferry` - republishes the transform of detected tools in tagslam so that
the pick-and-place pipeline knows where the tools are


### kraft_nui_moveit_config
This package contains the config files that allow us to control the
arm through MoveIt.
We typically don't run the launch files from this package directly, but
we include them in other launch files when we want to connect to the
arm's hardware
<!-- TODO: add reference for what this package is based on -->

#### Quirks
+ The arm's potentiometers tend to drift more than what MoveIt is currently set
to allow, which means that plans often fail mid-trajectory/won't compute at all
because the arm feedback has deviated sufficiently from what MoveIt expects.
Should theoretically be fixable by increasing tolerance to error

### kraft_ros_control_boilerplate
This package contains code for the low-level interfaces with the arm. This
package also contains the sub-package `kraft_control` which handles the
translation between our ROS messages and the RSD inputs/outputs.

To send commands to the RSD, it extracts the joint commands sent through ROS
with the associated PID values and creates a packet in the form the RSD expects.
The node also reads in packets from the RSD containing the current joint
potentiometer values and publishes those onto a ROS topic.

(Note: we determined the by plugging the mini-master and the RSD into a
repeating hub, and used a packet sniffer program like Wireshark to figure
out the format of the packet)
<!-- TODO: document packet form here -->
<!-- TODO: verify if this is true -->
<!-- TODO: was this based on another ROS package? If so, include the link here -->
<!-- TODO: add topic names -->
<!-- TODO: should we remove the old code? What are the copy files and the non _new files -->

#### Quirks
+ PID gains for the wrist are not set in the packet

### nlu_msgs
Deprecated? TODO: delete this

### rov_autonomy_master
This package contains the code creates the pilot's QT GUI used for interfacing
with the arm. The GUI sends commands to the arm via the pick-and-place pipeline,
so the code in this repo only contains code related to the visual interface
and not the arm's controls.
<!-- TODO: add screenshot of the GUI -->

### stereo_blinder
<!-- TODO: verify if this is true -->
This package contains a ROS node that subscribes to the stereo camera topic,
and creates a mask that hides the arm from view using the current transform
between each of the arm links and the camera. This utility is useful when
testing SLAM methods since the arm movement is independent from the
camera/vehicle's movement.
