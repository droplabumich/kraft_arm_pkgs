#!/usr/bin/env python3

# Python Imports 
from abc import abstractmethod
from threading import Thread
import yaml
import numpy as np

# ROS Imports
import rospy
import tf2_ros
import tf.transformations as tr
from geometry_msgs.msg import TransformStamped
from moveit_msgs.msg import RobotTrajectory, DisplayTrajectory
from std_msgs.msg import Bool, UInt8, String, Float32
from planner import Planner, MovePlanner
from kraft_arm_msgs.msg import ToolRequest, PoseRequest

# Relative Imports
from requests import PoseReq, GripReq, ToolCommandReq
import utils

# TODO: Remove hardcode
BASE_FRAME = "Base"
TOOL_FRAME = "detected_pushcore_1"
GRIPPER_FRAME = "Grip_Base"
PREV_GOAL_FRAME = "last_goal_pose"
QUIVER_FRAME = "detected_quiver_1"

# TODO: remove timing delay, add check for precision met
TIMING_DELAY = 5 # Number of seconds to wait between steps


class CommanderRemote():
    def __init__(self):
        rospy.init_node('rov_autonomy_remote')
        self.rate = rospy.Rate(10)

        self.curr_request = None
        self.last_cmd_time = 0
        self.initialized = False

        # Read yaml file to get info for pre-grasp poses
        filename = rospy.get_param("~pick_and_place_info")
        with open(filename, "r") as stream:
            self.pick_and_place_info = yaml.safe_load(stream)

    #     # Remote unity interface subscribers
    #     # rospy.Subscriber("/unity_interface/connected_scientists", String, self.unity_users_callback)

        # Remote unity interface publishers
        self.camera_fps_pub = rospy.Publisher("/unity_interface/camera_fps", Float32, queue_size=5)
        self.active_scientist_pub = rospy.Publisher("/unity_interface/active_scientist", String, queue_size=1)
        self.active_topics_pub = rospy.Publisher("/unity_interface/active_topics", String, queue_size=1)
        self.override_pub = rospy.Publisher("/unity_interface/remote_override", Bool, queue_size=1)
        self.data_enable_pub = rospy.Publisher("/unity_interface/data_enable", Bool, queue_size=1)
        self.override = False

        # Start up remote interface subscribers
        rospy.Subscriber("/unity_interface/request_plan", PoseRequest, self.unity_plan_req_callback)
        rospy.Subscriber("/unity_interface/request_tool_plan", ToolRequest, self.unity_tool_req_callback)
        rospy.Subscriber("/unity_interface/command_plan", PoseRequest, self.unity_command_callback) # TODO: make this a custom message with user ID
        rospy.Subscriber("/unity_interface/gripper_command", UInt8, self.unity_gripper_cmd_callback)
        rospy.Subscriber("/unity_interface/stop_request", Bool, self.unity_stop_request_callback)

        # Start up publishers/subscribers for planning/execution
        # TODO: add topic or tf listener to get tool pose
        # TODO: add topic for keys
        self.gripper_pub = rospy.Publisher("/kraft_arm/gripper", UInt8, queue_size=10)

        # Start up planner to handle all plan requests
        self.planning = Planner()
        # self.planning.clear_world()
        # self.planning.update_objects()
        self.planning_thread = Thread(target = self.planning.loop)
        self.planning_thread.start()

        # Start up planner to move arm
        self.move_planning = MovePlanner()
        # self.move_planning.clear_world()
        # self.move_planning.update_objects()
        self.move_planning_thread = Thread(target = self.move_planning.loop)
        self.move_planning_thread.start()

        # Start up tf listener 
        self.tf_buffer = tf2_ros.Buffer()
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer)
        self.tf_broadcaster = tf2_ros.TransformBroadcaster()

        # Start up main loop
        self.commander_thread = Thread(target = self.loop)
        self.commander_thread.start()

    def unity_plan_req_callback(self, msg):
        """Plan request callback
        @param msg: Plan request message C{kraft_arm_msgs/PoseRequest}
        """
        self.planning.queue_pose(msg)

    def unity_command_callback(self, msg):
        """Pose command callback
        @param msg: Pose command message C{kraft_arm_msgs/PoseRequest}
        """
        self.curr_request = PoseReq(msg) 

    def unity_gripper_cmd_callback(self, msg):
        """Gripper command callback
        @param msg: Gripper command message C{std_msgs/UInt8}
        """
        self.curr_request = GripReq(msg)  
        self.gripper_pub.publish(msg)


    def unity_tool_req_callback(self, msg):
        """Tool request callback
        @param msg: Tool request message C{kraft_arm_msgs/ToolRequest}
        """

        # TODO: use tool ID in msg instead of hardcoded frame in .yaml
        if msg.request == "pregrasp":
            rospy.loginfo("Received request to pregrasp tool " + msg.tool_name)
            self.curr_request = ToolCommandReq(msg, self.pick_and_place_info["tool_pregrasp"])
        elif msg.request == "pickup":
            rospy.loginfo("Received request to pick up tool " + msg.tool_name)
            self.curr_request = ToolCommandReq(msg, self.pick_and_place_info["tool_pickup"])
        elif msg.request == "return":
            rospy.loginfo("Received request to return tool " + msg.tool_name)
            self.curr_request = ToolCommandReq(msg, self.pick_and_place_info["tool_return"])
        else:
            rospy.logwarn("Unrecognized tool request " + msg.request + " for tool " + msg.tool_name)


    def unity_stop_request_callback(self, msg):
        self.curr_request = None
        self.move_planning.allstop()

    # # def unity_users_callback(self, msg):
    # #     pass 

    def lookup_transform(self, source_frame, target_frame):
        try:
            # Wait up to 1 second for transform update
            tf = self.tf_buffer.lookup_transform(source_frame, target_frame, rospy.Time(0), rospy.Duration(1))
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            rospy.logwarn("No transform found between " + source_frame + " and " + target_frame)
            return None
        return tf

    def process_grip_cmd(self, grip_cmd):
        return grip_cmd

    def process_pose_cmd(self, pose_cmd):
        # The des_pose needs to have the frame_id be the base frame,
        # with the child frame being the gripper frame (Note: for commands, 
        # the child_frame_id contains a unique command key, not the frame. 
        # The actual frame is assumed to be Grip_Base)

        # Notation:
        #   tf_PC - transform between parent and child of request
        #   tf_BT - transform between base and tool
        #   tf_FG - transform between last commanded gripper frame and desired gripper frame
        #   pc3_BP - tf matrix between base and parent
        #   pc3_BT - tf matrix between base and tool
        #   pc3_TG - tf matrix between tool and gripper
        #   pc3_BG - tf matrix between base and gripper frame

        tf_PC = pose_cmd.pose
        parent_frame_id = tf_PC.header.frame_id
        child_frame_id = tf_PC.child_frame_id

        # Direct pose commands: frame_id is base frame, child_frame_id is gripper. Name contains unique identifier
        if parent_frame_id == BASE_FRAME:
            # If the parent frame is already in the base frame, no math needed!
            des_pose = pose_cmd
            return des_pose
        
        # Tool pose commands: frame_id is gripper frame, child_frame_id is tool
        # Use tool pose as noted by request
        elif (parent_frame_id == GRIPPER_FRAME): 
            try:
                tf_BT = self.curr_request.req_msg.tool_transform
            except:
                rospy.logwarn("Error in finding tool transform in process_pose_cmd")
                return
            
            pc3_GT = utils.msg_to_se3(tf_PC) # Assume parent is gripper, child is tool
            pc3_BT = utils.msg_to_se3(tf_BT)

            pc3_TG = tr.inverse_matrix(pc3_GT)

            pc3_BG = tr.concatenate_matrices(pc3_BT, pc3_TG)

            des_pose = utils.create_pose_message(pc3_BG, BASE_FRAME, GRIPPER_FRAME, rospy.Time.now(),
                                                 pose_cmd.name, pose_cmd.pos_tolerance, pose_cmd.rot_tolerance)
               
            return des_pose

        # Servoing commands: frame_id is last goal pose, child_frame_id is gripper 
        elif (parent_frame_id == PREV_GOAL_FRAME) and (child_frame_id == GRIPPER_FRAME): 
            tf_BF = self.lookup_transform(BASE_FRAME, PREV_GOAL_FRAME)
            if tf_BF == None:
                return

            pc3_FG = utils.msg_to_se3(tf_PC) # Assume parent is last goal pose, child is gripper
            pc3_BF = utils.msg_to_se3(tf_BF)

            pc3_BG = tr.concatenate_matrices(pc3_BF, pc3_FG)

            des_pose = utils.create_pose_message(pc3_BG, BASE_FRAME, GRIPPER_FRAME, rospy.Time.now(),
                                                 pose_cmd.name, pose_cmd.pos_tolerance, pose_cmd.rot_tolerance)
            return des_pose
        # Relative commands (used for tool return): frame_id is the quiver, child_frame_id is the tool
        elif (parent_frame_id == QUIVER_FRAME) and (child_frame_id == TOOL_FRAME): 
            # Use this code to rely on arm feedback for tool position. Assumes tool grasp is fixed during return 
            try:
                tf_G_T = self.curr_request.req_msg.tool_transform # Calibrated gripper to tool tf
                tf_B_Q = self.curr_request.req_msg.quiver_transform # Calibrated tf between base and quiver
            except:
                rospy.logwarn("Error in reading quiver transform from request message in process_pose_cmd")
                return
                        
            pc3_Q_T   = utils.msg_to_se3(tf_PC) # Assume parent is quiver, child is tool
            pc3_G_T = utils.msg_to_se3(tf_G_T)
            pc3_B_Q    = utils.msg_to_se3(tf_B_Q)

            pc3_B_G = tr.concatenate_matrices(pc3_B_Q, pc3_Q_T, tr.inverse_matrix(pc3_G_T))

            des_pose = utils.create_pose_message(pc3_B_G, BASE_FRAME, GRIPPER_FRAME, rospy.Time.now(),
                                                 pose_cmd.name, pose_cmd.pos_tolerance, pose_cmd.rot_tolerance)
            return des_pose

            # Use this code to continously look up transform between gripper and tool during return -----------
            # try:
            #     tf_B_Q = self.curr_request.req_msg.quiver_transform # Calibrated tf between base and quiver
            # except:
            #     rospy.logwarn("Error in reading quiver transform from request message in process_pose_cmd")
            #     return
            # try:
            #     tf_B_G = self.lookup_transform(BASE_FRAME, GRIPPER_FRAME)
            # except:
            #     rospy.logwarn("Error in finding transform between base and gripper in process_pose_cmd")
            #     return
            # try:
            #     tf_B_Tmes = self.lookup_transform(BASE_FRAME, TOOL_FRAME)
            # except:
            #     rospy.logwarn("Error in finding transform between base and tool in process_pose_cmd")
            #     return
            
            # pc3_Q_Tdes   = utils.msg_to_se3(tf_PC) # Assume parent is quiver, child is tool
            # pc3_B_Tmes = utils.msg_to_se3(tf_B_Tmes)
            # pc3_B_Q    = utils.msg_to_se3(tf_B_Q)
            # pc3_B_G    = utils.msg_to_se3(tf_B_G)

            # pc3_G_Tmes = tr.concatenate_matrices(tr.inverse_matrix(pc3_B_G), pc3_B_Tmes)

            # # We want the transform between gripper and tool to stay consistent 
            # # when moving the tool to to the desired position 
            # pc3_Tdes_Gcmd = tr.inverse_matrix(pc3_G_Tmes) 
            
            # # Compute gripper command in base frame
            # pc3_B_Gcmd = tr.concatenate_matrices(pc3_B_Q, pc3_Q_Tdes, pc3_Tdes_Gcmd)
            # des_pose = utils.create_pose_message(pc3_B_Gcmd, BASE_FRAME, GRIPPER_FRAME, rospy.Time.now(),
            #                                      pose_cmd.pos_tolerance, pose_cmd.rot_tolerance)
            # return des_pose

        else:
            rospy.logwarn("Unrecognized pose command type in yaml")
            return        

    def loop(self):
        while not rospy.is_shutdown():
            # TODO: Remove hardcode - These need a gui interface
            if not self.initialized:
                self.rate.sleep()
                camera_fps_msg = Float32(10)
                self.camera_fps_pub.publish(camera_fps_msg)
                active_scientist_msg = String("DEVELOPER (shore)")
                self.active_scientist_pub.publish(active_scientist_msg)
                active_topics_msg = String("/left_img_topic,/fish_img_topic,/rgbd_img_topic,/pcl_topic")
                self.active_topics_pub.publish(active_topics_msg)
                override_msg = Bool(True)
                self.override = True
                self.override_pub.publish(override_msg)
                data_enable_msg = Bool(True)
                self.data_enable_pub.publish(data_enable_msg)
                self.initialized = True

            des_grip, des_pose = None, None

            if self.curr_request != None:
                obj_pose, obj_grip = self.curr_request.poll_objective()
        
                if obj_grip != None and self.override:
                    des_grip = self.process_grip_cmd(obj_grip)

                if obj_pose != None and self.override:
                    des_pose = self.process_pose_cmd(obj_pose)

                if (obj_pose == None) and (obj_grip == None):
                    self.rate.sleep()
                    continue

            # Ensure enough time has passed since last command
            if (rospy.Time.now().to_sec() - self.last_cmd_time > self.move_planning.last_trajectory_length + TIMING_DELAY): 
                if des_grip != None:
                    self.gripper_pub.publish(des_grip)
                    self.last_cmd_time = rospy.Time.now().to_sec()

                if des_pose != None and self.move_planning.available:
                    preview_tf = des_pose.pose
                    preview_tf.child_frame_id = PREV_GOAL_FRAME
                    self.tf_broadcaster.sendTransform(preview_tf)

                    self.move_planning.move_to_pose(des_pose)
                    self.last_cmd_time = rospy.Time.now().to_sec()

                # If conditions met (TODO: check tolerances)
                if (des_grip or des_pose):
                    self.curr_request.inc_step()

            self.rate.sleep()


    def __del__(self):
        self.planning_thread.join()
        self.commander_thread.join()

if __name__ == '__main__':
    cmd = CommanderRemote()
    cmd.loop()





"""
TODO: Left off here
current goal: get to the point where pregrasps work with no feedback (just move there using available flag)

what I was doing last: checking for child frame ID: how do I know when I  need to look up tf and compute goal pose


Input:
frame_id: unique identifier of request
child_frame_id: Grip_Base (i think, unverified)

"""