#!/usr/bin/env python3

# Python Imports 
from abc import abstractmethod

# ROS Imports
from geometry_msgs.msg import TransformStamped, Quaternion
from kraft_arm_msgs.msg import PoseRequest
import tf.transformations as tr

# Custom Imports
import utils


class SequentialReq():
    def __init__(self):
        self.curr_step = 0

    def inc_step(self):
        self.curr_step += 1

    @abstractmethod
    def poll_objective(self):
        """Get pose goal, gripper goal, positioning tolerance, and 
        rotational tolerance based on the current step in the request
        
        Output format should be pose (PoseRequest), gripper command (UInt8)
        position threshold (float, in meters), rotation threshold (float, in 
        radians)
        """
        raise NotImplementedError("Request objectives not specified")

class PoseReq(SequentialReq):
    """Pose Request - requires desired pose input as PoseRequest
    message
    """

    def __init__(self, pose):
        super(PoseReq, self).__init__()
        self.pose_cmd = pose

    def poll_objective(self):
        # Pose command
        if self.curr_step == 0:
            return self.pose_cmd, None

        # Done state
        else:
            return None, None

class GripReq(SequentialReq):
    """Grip Request - requires desired grip input as UInt8
    messsage
    """

    def __init__(self, grip_cmd):
        super(GripReq, self).__init__()
        self.grip_cmd = grip_cmd

    def poll_objective(self):
        # Gripper command
        if self.curr_step == 0:
            return None, self.grip_cmd

        # Done state
        else:
            return None, None

class StructuredReq(SequentialReq):
    """Request dictated by yaml"""

    def __init__(self, req_info):
        super(StructuredReq, self).__init__()
        self.req_info = req_info
    
    def poll_objective(self):
        name = None
        pose_cmd = None
        grip_cmd = None
        pos_thresh = None
        rot_thresh = None

        for i, step_info in enumerate(self.req_info):
            if i == self.curr_step:
                if "name" in step_info.keys():
                    name = step_info["name"]
                if "gripper" in step_info.keys():
                    grip_cmd = step_info["gripper"]
                if "pos_thresh" in step_info.keys():
                    pos_thresh = step_info["pos_thresh"]
                if "rot_thresh" in step_info.keys():
                    rot_thresh = step_info["rot_thresh"]
                if "pose" in step_info.keys():
                    # Create transformStamped 
                    # TODO: put this code in utils, make it less janky
                    trans = step_info["pose"]["translation"]
                    rot = step_info["pose"]["rotation"]
                    tf_stamped = TransformStamped()
                    tf_stamped.header.frame_id = step_info["pose"]["parent_frame_id"]
                    tf_stamped.child_frame_id = step_info["pose"]["child_frame_id"]
                    tf_stamped.transform.translation.x = trans["x"]
                    tf_stamped.transform.translation.y = trans["y"]
                    tf_stamped.transform.translation.z = trans["z"]

                    quat = Quaternion(*tr.quaternion_from_euler(rot["x"], rot["y"], rot["z"]))
                    tf_stamped.transform.rotation = quat

                    pose_cmd = PoseRequest(tf_stamped, name, pos_thresh, rot_thresh)
        # Done state - we'll reach this if the current step is greater than i
        return pose_cmd, grip_cmd


class ToolCommandReq(StructuredReq):
    """Tool Request - requires desired name of tool as String
    message and pose info from yaml file

    @param req_msg: Tool request message C{kraft_arm_msgs/ToolRequest}
    @param req_info: Request info C{dict} 
    """

    def __init__(self, req_msg, req_info):
        super(ToolCommandReq, self).__init__(req_info)
        self.req_msg = req_msg