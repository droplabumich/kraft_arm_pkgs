#!/usr/bin/env python3

import sys
import queue
import rospy

import moveit_commander
from std_msgs.msg import String
from geometry_msgs.msg import PoseStamped
from moveit_msgs.msg import DisplayTrajectory

import threading 

mutex = threading.Lock()

# TOPIC_DTP = "move_group/display_planned_path"
TOPIC_DTP_REMOTE = "/unity_interface/trajectory"
TOPIC_STATUS = "/unity_interface/status"

DEFAULT_POS_TOLERANCE = 0.003
DEFAULT_ROT_TOLERANCE = 0.01


class Planner():
    """Handles motion planning. Constructed to run in thread to ensure 
    planning time does not interfere with other processes. Assumes ROS 
    node is already initialized
    """

    def __init__(self):
        moveit_commander.roscpp_initialize(sys.argv)
        self.robot = moveit_commander.RobotCommander()
        self.scene = moveit_commander.PlanningSceneInterface()
        self.group = moveit_commander.MoveGroupCommander("arm")

        self.group.set_goal_position_tolerance(DEFAULT_POS_TOLERANCE)
        self.group.set_goal_orientation_tolerance(DEFAULT_ROT_TOLERANCE)
        self.group.set_max_velocity_scaling_factor(1.0)
        self.group.set_max_acceleration_scaling_factor(1.0)

        self.pose_request_queue = queue.Queue() # Queue module ensures safe info transfer 
                                                # between threads
                                                # https://docs.python.org/3/library/queue.html

        # Publishers 
        # self.display_trajectory_pub = rospy.Publisher(TOPIC_DTP, DisplayTrajectory, queue_size=1)
        self.remote_trajectory_pub = rospy.Publisher(TOPIC_DTP_REMOTE, DisplayTrajectory, queue_size=1)
        self.status_pub = rospy.Publisher(TOPIC_STATUS, String, queue_size=10)

        # Info vars 
        self.last_trajectory_length = 0
        self.last_plan = None

    def queue_pose(self, pose):
        """Queue pose position for planning. We use a queue to avoid having
        multiple threads request a plan simultaneously. Queued plans are not 
        executed, they are simply created for visualization purposes
        
        Args:
            pose (PoseRequest): pose to plan end-effector to. Field
                child_frame_id is used to store unique ID. Actual child frame
                is assumed to be the gripper frame 
        
        TODO: use name to store ID instead of child frame
        """
        req = pose
        self.pose_request_queue.put(req)

    def _plan_cartesian_path(self, tf_msg, name, pos_tolerance, rot_tolerance):
        """Calls MoveIt to plan a cartesian path to specified pose. 

        NOTE: Do not call this function in a callback function

        Args:
            pose (TransformStamped): pose to plan end-effector to. Field
                child_frame_id is used to store unique ID. Actual child frame
                is assumed to be the gripper frame 
        """
        # Convert from TransformStamped to PoseStamped
        pose = PoseStamped()
        pose.header = tf_msg.header
        pose.pose.orientation = tf_msg.transform.rotation
        pose.pose.position.x = tf_msg.transform.translation.x
        pose.pose.position.y = tf_msg.transform.translation.y
        pose.pose.position.z = tf_msg.transform.translation.z

        group = self.group
        group.clear_pose_targets()
        self.group.allow_replanning(False)
        self.group.set_start_state_to_current_state()
        self.group.set_pose_reference_frame(pose.header.frame_id)
        group.set_pose_target(pose)
        if pos_tolerance != 0:
            group.set_goal_position_tolerance(pos_tolerance)
        else:
            group.set_goal_position_tolerance(DEFAULT_POS_TOLERANCE)
        if rot_tolerance != 0:
            group.set_goal_orientation_tolerance(rot_tolerance)
        else:
            group.set_goal_orientation_tolerance(DEFAULT_ROT_TOLERANCE)
        
        #self._publish_status("pose plan running", name)
        (success, plan, planning_time, error_code) = group.plan()
        group.clear_path_constraints()

        output = plan, success

        if success:
            self._publish_status("pose plan complete", name)
            self.last_trajectory_length = plan.joint_trajectory.points[-1].time_from_start.to_sec()
            self.last_plan = plan
        else:
            print("Plan failed: {}".format(error_code))
            self._publish_status("pose plan failed", name)
            self.last_trajectory_length = 0
            self.last_plan = None
            output = None, success 

        return output

    def _publish_trajectory(self, plan, key):
        """Publishes custom display trajectory message for Unity. Puts 
        unique request ID in model_id field of trajectory message
        """
        traj_msg = DisplayTrajectory()
        traj_msg.model_id = key # Put request ID in model_id
        traj_msg.trajectory_start = self.group.get_current_state()
        traj_msg.trajectory.append(plan)
        self.remote_trajectory_pub.publish(traj_msg)

    def _publish_status(self, status, key):
        msg = String(f"moveit {key} {status}")
        self.status_pub.publish(msg)

    def loop(self):
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():           
            # If we have a plan request, act on it
            if not self.pose_request_queue.empty():
                # Read next plan request from queue
                req = self.pose_request_queue.get()
                plan, success = self._plan_cartesian_path(req.pose, req.name, req.pos_tolerance, req.rot_tolerance)
                if success:
                    self._publish_trajectory(plan, req.name) 
            rate.sleep()

class MovePlanner(Planner):
    """Handles motion planning and trajectory execution"""

    def __init__(self):
        self.available = True
        self.req = None
        Planner.__init__(self)

    def allstop(self):
        self.group.stop()
        self.group.clear_pose_targets()

    def move_to_pose(self, pose):
        """Attempts to plan a path to the specified pose and execute
        the plan"""
        self.req = pose

   
    def _execute_plan(self, plan, key, wait=True):
        """Calls MoveIt to move arm along pre-computed trajectory

        NOTE: Do not call this function in a callback function
        """
        if plan is not None:
            self.group.set_start_state_to_current_state()
            try:
                #self._publish_status("pose execute running", key)
                # TODO: execute should return true or false, but our path
                # tolerances are off and so this always returns false.
                # To make this more robust, we should fix this behavior
                self.group.execute(plan, wait=wait)
                #self._publish_status("pose execute success", key)
            except:
                #self._publish_status("pose execute failed", key)
                pass
            self.group.stop()
            self.group.clear_pose_targets()

    def loop(self):
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            if self.req != None:
                self.available = False
                mutex.acquire() # Lock thread to ensure our request doesn't unexpectedly change
                plan, success = self._plan_cartesian_path(self.req.pose, self.req.name, self.req.pos_tolerance, self.req.rot_tolerance)
                if success:
                    self._publish_trajectory(plan, self.req.name)  
                    self._execute_plan(plan, self.req.name)
                self.req = None
                mutex.release() # Unlock thread
                self.available = True
            rate.sleep()